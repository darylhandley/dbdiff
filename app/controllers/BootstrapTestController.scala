package controllers

import play.api._
import play.api.mvc._
import web.ProvidesHeader

object BootstrapTestController extends Controller with ProvidesHeader {
  

	def index = Action { implicit request =>
    Ok(views.html.bootstrapTest("null not used"))
	}


}