

def printSomething(something : String) = println(something)
printSomething("Hello")

// var printSomething2  = printSomething("Here is something " + _)

// function literal
var increase = (x : Int) => x + 1
println(increase(10))


var printSomething2  = (something: String) => println("Something2 :: " + something)
printSomething2("Hello")

var printSomething3  = (something: String) => {
  println("=============================")
  println("Something2 :: " + something)
  println("=============================")
}
printSomething3("Hello")


val someStuff = List("hello", "world", "how", "goes", "it")

someStuff.foreach(printSomething)

someStuff.foreach(printSomething3)

someStuff.foreach(println (_))






