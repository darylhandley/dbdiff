# --- !Ups

CREATE SEQUENCE competitor_id_seq;
CREATE TABLE competitor (
  id integer NOT NULL UNIQUE DEFAULT nextval('competitor_id_seq') ,
  league_id varchar(10)  REFERENCES league (id),
  code varchar(10) NOT NULL,
  name varchar(100) NOT NULL
);

insert into competitor (league_id, code, name) values ('NHL', 'VAN', 'Vancouver canucks');

# --- !Downs

DROP TABLE competitor;
DROP SEQUENCE competitor_id_seq;