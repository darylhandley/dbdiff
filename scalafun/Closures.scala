
var suffix = " Dude!!"

def oneDimensionalCharacter = (s: String) => {
  println(s + suffix)
}

oneDimensionalCharacter("Hello")
oneDimensionalCharacter("What's happening")

suffix = " my good sir"

oneDimensionalCharacter("Hello ")
oneDimensionalCharacter("Top of the morning to you,")

def createOneDimensionalCharacter(s1: String) = (s2: String) => {
  println(s2 + s1)
}

val loudSurfer = createOneDimensionalCharacter(" DUDE !!!")

val doorman = createOneDimensionalCharacter(" my good sir.")

loudSurfer("yo")

doorman("Please keep the noise level down, thank you")


