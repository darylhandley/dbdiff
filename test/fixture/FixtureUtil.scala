package fixture

import javax.sql.DataSource

import org.apache.commons.dbcp.BasicDataSource
import play.api.db.DB
import anorm._
import play.api.Play.current
import play.api.test.FakeApplication
import org.squeryl.annotations._
import anorm.Column
import java.util.Date
import java.sql.Connection

import test.TestConstants

/**
 * Created with IntelliJ IDEA.
 * User: daryl
 * Date: 2013-09-02
 * Time: 1:28 PM
 * To change this template use File | Settings | File Templates.
 */
class FixtureUtil {

}

object FixtureUtil {

  val queries = List(
    "delete from comparison",
    "delete from datastore",
    "delete from version",

    // version
    "insert into version (id, name)  values (1, '1.2.3')",
    "insert into version (id, name)  values (2, '1.2.4')",
    "insert into version (id, name)  values (3, '1.2.5')",

    // datastore
    "insert into datastore (id, name, host, port, username, password, db_name, version_id, is_reference) " +
      " values (1, 'Test 1.2.3 Reference', '127.0.0.1', 9999, 'testuser', 'testpassword', 'test_ref', 1, true)",
    "insert into datastore (id, name, host, port, username, password, db_name, version_id, is_reference) " +
      " values (2, 'Test 1.2.3 Prod db1', '127.0.0.2', 9999, 'testuser', 'testpassword', 'test_prod_1', 1, false)",
    "insert into datastore (id, name, host, port, username, password, db_name, version_id, is_reference) " +
      " values (3, 'Test 1.2.3 Prod db2', '127.0.0.3', 9999, 'testuser', 'testpassword', 'test_prod_2', 1, false)",
    "insert into datastore (id, name, host, port, username, password, db_name, version_id, is_reference) " +
      " values (4, 'Test 1.2.4 Reference', '127.0.0.3', 9999, 'testuser', 'testpassword', 'test_ref_1.2.4', 2, true)",
    "insert into datastore (id, name, host, port, username, password, db_name, version_id, is_reference) " +
      " values (5, 'Test 1.2.4 Prod db1', '127.0.0.1', 9999, 'testuser', 'testpassword', 'test_prod_1_1.2.4', 2,  false)",

    // comparison
    "insert into comparison (id, reference_datastore_id, compare_datastore_id, status, details, created_time, start_time, completed_time) " +
      " values (1, 1, 2, 'PASSED', 'the details', '2015-01-01 15:37:34', '2015-01-01 16:37:34', '2015-01-01 17:37:34' )",
    "insert into comparison (id, reference_datastore_id, compare_datastore_id, status, details, created_time, start_time, completed_time) " +
      " values (2, 1, 2, 'PASSED', 'the details', '2015-01-01 15:37:34', null, null)",

      // set sequences
    "ALTER sequence datastore_id_seq RESTART WITH 100",
    "ALTER sequence version_id_seq RESTART WITH 100",
    "ALTER sequence comparison_id_seq RESTART WITH 100"

  )

  def resetDatabase {
    DB.withConnection { implicit connection =>
      queries.foreach(query =>
        SQL(query).execute()
      )
    }
  }

  def createComparisonDatabases: Unit = {

    // create datasource
    val dataSource: BasicDataSource = {
      val ds = new BasicDataSource
      ds.setDriverClassName("org.postgresql.Driver")
      ds.setUsername("darylhandley")
      ds.setPassword("")
      ds.setUrl("jdbc:postgresql://localhost")
      ds
    }

    // get a connection
    implicit val conn = dataSource.getConnection()

    createCompareDatabase(TestConstants.DBDIFF_TEST_COMPARE_REF, conn)
    createCompareDatabase(TestConstants.DBDIFF_TEST_COMPARE_COMP, conn)

    // close connection
    conn.close()
    dataSource.close()


  }

  def createCompareDatabase(dbName : String, conn : Connection): Unit = {

    implicit var myConn = conn

    // drop all connections
    SQL(s"SELECT pg_terminate_backend(pg_stat_activity.pid) FROM pg_stat_activity " +
      s"WHERE datname = '$dbName' AND pid <> pg_backend_pid();").execute()

    // drop database if it exists
    SQL(s"drop database if exists $dbName").execute()

    // create database
    SQL(s"create database $dbName").execute()

    // connect to newly created db for the rest of the queries
    // create datasource
    val dataSource: DataSource = {
      val ds = new BasicDataSource
      ds.setDriverClassName("org.postgresql.Driver")
      ds.setUsername("darylhandley")
      ds.setPassword("")
      ds.setUrl("jdbc:postgresql://localhost/" + dbName)
      ds
    }

    // change our implicit conn to be the newly created
    myConn = dataSource.getConnection()

    // add person table
    val createTableSql = """create table person (
                           	id bigint not null,
                           	first_name varchar default 'daryl',
                           	last_name varchar(40),
                            money_in_pocket numeric (3, 2) default 1.25,
                           	primary key(id)
                           )"""

    SQL(createTableSql).execute()

  }




}
