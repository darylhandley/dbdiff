package views

import views.html.bootstrapFieldConstructorTemplate

object TwitterBootstrapHelper {
  import views.html.helper.FieldConstructor
  implicit val myFields = FieldConstructor(bootstrapFieldConstructorTemplate.f)
}
