package controllers

import play.api.mvc._
import play.api.libs.iteratee.Enumerator
import web.ProvidesHeader

/**
 * The front page for the project.
 */
object HomeController extends Controller with ProvidesHeader {
  
  def index = Action { implicit request =>
  	    // Ok(views.html.main("BetBaron", "Your new application is ready mother fucker."))
    Ok(views.html.home("dbDiff"))
  }
}