package controllers

import play.api.data.Form
import play.api.data.Forms._
import views.html
import play.Logger

import model._
import play.api.mvc._
import web.ProvidesHeader

/**
 *
 */
object DatastoresController extends Controller with ProvidesHeader {

  def list = Action { implicit request =>
    Logger.info("Displaying all datastores")
    val datastores = Datastore.all().sortBy(_.id)
    Ok(views.html.datastores("Datastores", datastores))
  }


  val addEditDatastoreForm: Form[Datastore] = Form(

    // define mapping to handle datastore  values
    mapping(
      "id" -> optional(longNumber()),
      "name" -> nonEmptyText,
      "host" -> nonEmptyText,
      "port" -> number(),
      "username" -> nonEmptyText,
      "password" -> text,
      "dbName" -> nonEmptyText,
      "versionId" -> number(),
      "isReference" -> boolean

      // tuple for password and confirm
      //      "password" -> tuple(
      //        "main" -> text(minLength = 6),
      //        "confirm" -> text
      //      ).verifying(
      //        "Confirm password does not match password.", passwords => passwords._1 == passwords._2
      //      )
    )
      // The mapping signature doesn't match the Player case class signature,
      // so we have to define custom binding/unbinding functions

    {
      // Binding: Create a datastore from the mapping result
      // it gets username, email, passwords from the mapping above, not sure how
      (id, name, host, port, username, password, dbName, versionId, isReference)
        => Datastore(id.getOrElse(0), name, host, port, username, password, dbName, versionId, isReference)
    }
    {
      // Unbinding: Create the mapping values from an existing datastore value
      datastore => Some(Some(datastore.id), datastore.name, datastore.host, datastore.port, datastore.username, datastore.password,
        datastore.dbName, datastore.versionId, datastore.isReference)
    }.verifying(
        // this sets a global error but we would like to set an error on the isReference field, not sure how at the moment
        // see this later to see if you can get it to work
        // http://stackoverflow.com/questions/12100698/play-framework-2-0-validate-field-in-forms-using-other-fields
        "A reference datastore already exists for this version",
        datastore => {
          Logger.info("Verfiying reference datastore does not exist. id = " + datastore.id +
            ", versionId= " + datastore.versionId + " ,isReference=" + datastore.isReference)
          !(datastore.isReference && Datastore.referenceDatastoreExists(datastore.versionId, datastore.id))
        }
      )
  )

  def create = Action { implicit request =>
    val datastore = Datastore(0, "", "", 5432, "", "", "", 0, false)
    val prefilledForm = addEditDatastoreForm.fill(datastore)
    Ok(views.html.datastoresEdit(prefilledForm, "Add Datastore"))
  }

  def edit (id: Long) = Action { implicit request =>
    // TODO, check if the datastore exists
    val datastore = Datastore.getById(id)
    val prefilledForm = addEditDatastoreForm.fill(datastore.get)
    Ok(views.html.datastoresEdit(prefilledForm, "Edit Datastore"))
  }

  /** prefills the form with a copy of the selected datasource which can then be edited and saved */
  def copy (id: Long) = Action { implicit request =>
    // TODO, check if the datastore exists
    val datastore = Datastore.getById(id)
    val prefilledForm = addEditDatastoreForm.fill(datastore.get.copy(id = 0L))
    Ok(views.html.datastoresEdit(prefilledForm, "Add Datastore"))
  }

  def save = Action { implicit request =>
    addEditDatastoreForm.bindFromRequest.fold(

      // Form has errors, redisplay it
      errors => BadRequest(html.datastoresEdit(errors, "An Error Occurred !")),

      // We got a valid datastore value, save it and display the summary
      datastore => {
        datastore.id match {
          //          case Some(Long) => {
          //            Datastore.update(datastore)
          //          }
          case 0 => {
            Datastore.insert(datastore)
          }
          case _ => {
            Datastore.update(datastore)
          }
        }

        // Player.create(player.username, player.password, player.email)
        // Ok(html.addEditDatastoreSuccess("Datastore Saved", datastore))
        Redirect(routes.DatastoresController.list())
      }
    )
  }

  def delete (id: Long) = Action { implicit request =>
    // TODO, check if the datastore exists
    val datastore = Datastore.delete(id)
    Redirect(routes.DatastoresController.list())
  }
}