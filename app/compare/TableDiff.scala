package compare

import compare.TableDiffType.TableDiffType

/**
 * Created by darylhandley on 15-05-11.
 *     - TableDiff
 *        - tableName : String (with schema ex: schema1.custoemer)
 *        - tableDiffType - TableDiffType: MISISNG_IN_REF_DS, MISSING_IN_COMP_DS, STRUCTURAL
 *      - columnDiffs : List<ColumnDiffs>
 *          - ColumnDiff
 *            - columnName : String
 *            - columnDiffType: ColumnDiffType (NULLABILITY, DATATYPE, DATATYPE, DEFAULT, PRIMARY_KEY)
 *            - refDbValue : String
 *            - compareDbValue : String
 *      - ordinalityDiff : boolean
 *      - refTableOrdinality : Option[String]
 *      - compTableOrdinality : Option[String]
 */
case class TableDiff (
                  tableDef : TableDef,
                  tableDiffType : TableDiffType,
                  columnDiffs : List[ColumnDiff],
                  ordinalityDiff : Boolean,
                  refTableOrdinality : Option[String],
                  compTableOrdinality : Option[String],
                  primaryKeyDiff : Boolean,
                  refPrimaryKey : Option[String],
                  compPrimaryKey  : Option[String]
) {

//  val tableName = tableNameParam
//  val tableDiffType = tableDiffTypeParam
//  val columnDiffs = columnDiffsParam
//  val ordinalityDiff = ordinalityDiffParam
//  val refTableOrdinality = refTableOrdinalityParam
//  val compTableOrdinality = compTableOrdinalityParam



}

object TableDiffType extends Enumeration {
  type TableDiffType = Value
  val IN_REF_ONLY, IN_COMPARE_ONLY, STRUCTURE_DIFF = Value

}
