package learn

/**
 * Created by darylhandley on 15-05-15.
 */
class LearnClassesAndCasts {

}

object LearnClassesAndCasts {

  def printWhatItIs(any: Any): Unit = {
    println(any.getClass)
  }

  def printWhatItIsAnyRef(ref: AnyRef): Unit = {
    println(ref.getClass)
  }


  def main (args : Array[String]): Unit = {

    val myString = "test"
    val myRef : AnyRef = myString
    println(myRef)

    printWhatItIs(myString)
    printWhatItIs(1)
    printWhatItIs(1L)

    // second 2 do not compile
    // not sure what the difference between AnyRef and Any is
    printWhatItIsAnyRef(myString)
    // printWhatItIsAnyRef(1)
    //printWhatItIsAnyRef(1L)




  }
}
