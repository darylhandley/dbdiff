package jobs

import java.sql.Timestamp
import java.util.Date

import akka.actor.Actor
import compare.{CompareResultFormatter, PostgresCompare}
import model.{ComparisonStatus, Comparison}
import play.Logger


object ComparisonRunner {
  case object Greet
  case object Done
}

class ComparisonRunner extends Actor {
  def receive = {
    case ComparisonRunner.Greet => {
      Logger.info("********************************** inside the receive method    **********************")
      sender ! ComparisonRunner.Done
    }
    case _ => {
      Logger.info("*********************** default case encountered *********************")

      val comparisons = Comparison.allPending()

      Logger.info("Executing Comparison Job.")

      if (comparisons.size > 0) {
        Logger.info("Processing " + comparisons.size)
        comparisons.foreach(processComparison(_))

      } else {
        Logger.info("No pending comparisons found")
      }





    }
  }

  def processComparison(comparison : Comparison): Unit = {
    // set into running status
    Logger.info("set to running status")
    var comparisonCopy = comparison.copy(
      status = ComparisonStatus.RUNNING,
      startTime = Some(new Timestamp(new Date().getTime()))
    )
    Comparison.update(comparisonCopy)


    // execute the comparison
    val compareResult = PostgresCompare.compare(comparison.referenceDatastore, comparison.compareDatastore)
    val newStatus = if (compareResult.hasDifferences) ComparisonStatus.FAILED else ComparisonStatus.PASSED

    val details = if (compareResult.hasDifferences) {
      CompareResultFormatter.format(compareResult)
    } else {
      "No differences found"
    }

    comparisonCopy = comparisonCopy.copy(
      status = newStatus,
      completedTime = Some(new Timestamp(new Date().getTime())),
      details = details
    )
    Comparison.update(comparisonCopy)


  }
}