package model

import model.Database._
import org.squeryl.KeyedEntity
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.annotations._

/**
 * Created by darylhandley on 15-04-27.
 */
case class Version (
   id : Long,
   @Column("name") name : String
   ) extends KeyedEntity[Long] {

}

object Version {
  def getById(id: Long): Option[Version] = inTransaction {
    versionTable.lookup(id)
  }

  def insert(version: Version): Version = inTransaction {
    versionTable.insert(version)
  }

  def delete(id: Long): Boolean = inTransaction {
    versionTable.delete(id)
  }

  def update(version: Version) {
    inTransaction {
      versionTable.update(version)
    }
  }

  def all()  = inTransaction {
    from(versionTable) {datastore => select(datastore)}.toList
  }

}
