# --- !Ups
ALTER TABLE "public"."event"
	ADD COLUMN "sportsbook_id" integer NOT NULL default 0;


# --- !Downs
ALTER TABLE "public"."event"
	drop COLUMN "sportsbook_id";
