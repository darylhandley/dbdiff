package compare

import java.util
import javax.sql.DataSource
import java.sql.Connection

import _root_.util.DataSourceUtil
import anorm._
import compare.ColumnDiffType._
import compare.TableDiffType.TableDiffType
import model.Datastore
import org.apache.commons.dbcp.BasicDataSource
import play.Logger

import scala.collection.immutable.HashSet
import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/*
 * Compares the database for 2 Datastores.
 */
object PostgresCompare {

  def compare(refDatastore: Datastore, compDatastore: Datastore): CompareResult = {

    val refDataSource = DataSourceUtil.createDataSourceFromDataStore(refDatastore)
    val compDataSource = DataSourceUtil.createDataSourceFromDataStore(compDatastore)
    val refConn = refDataSource.getConnection()
    val compConn = compDataSource.getConnection()

    // get table diffs
    val tableDiffs = compareTables(refConn, compConn)

    // close connections and datasources
    refConn.close()
    compConn.close()
    refDataSource.close()
    compDataSource.close()

    val compareResult = new CompareResult()
    compareResult.tableDiffs = tableDiffs

    compareResult
  }

  def compareTables(refConn: Connection, compConn: Connection): List[TableDiff] = {

    // read tables from the datastores
    val refTables = loadTables(refConn)
    val compTables = loadTables(compConn)
    val allTables = refTables ++ compTables

    // put all the table names into a sorted set
    val refTableNames = refTables.map(table => table.schema + "." + table.tableName).toSet
    val compTableNames = compTables.map(table => table.schema + "." + table.tableName).toSet
    val allTableNames : Set[String] = refTableNames ++ compTableNames

    def missingTableDiff(tableDef : TableDef, diffType: TableDiffType) = {
      new TableDiff(tableDef,
        diffType,
        List[ColumnDiff](),
        false,
        None,
        None,
        false,
        None,
        None
      )
    }

    val tableDiffs = ListBuffer[TableDiff]()

    // for each extra table in the comp db add a tableDiff to tableDiffs
    val compExtraTables = allTables -- refTables
    compExtraTables.foreach(tableDiffs += missingTableDiff(_, TableDiffType.IN_COMPARE_ONLY))

    // for each extra table in the comp db add a tableDiff to tableDiffs
    val refExtraTables = allTables -- compTables
    refExtraTables.foreach(tableDiffs += missingTableDiff(_, TableDiffType.IN_REF_ONLY))

    // iterate over the tables in both and find structural diffs
    val tablesInBoth = allTables -- refExtraTables -- compExtraTables
    tablesInBoth.foreach(tableDef => {
      val tableDiff = findTableStructureDiff(tableDef, refConn, compConn)
      if (tableDiff.isDefined) {
        tableDiffs += tableDiff.get
      }
    })

    // sort by schema.tableName
    tableDiffs.toList.sortBy(_.tableDef.fullName)

  }


  /*
   * find structural differences between table in ref and table in comp.
   */
  def findTableStructureDiff(tableDef: TableDef, refConn : Connection, compConn : Connection) : Option[TableDiff] = {

    def listToMap (columnDefs : List[ColumnDef]) =  {
      columnDefs.map(columndDef => columndDef.column_name -> columndDef).toMap
    }

    // read the columns from each table into both a list and a map
    val refColumnDefs  = loadColumnDefs(refConn, tableDef.schema, tableDef.tableName)
    val refColumnDefsMap = listToMap(refColumnDefs)
    val compColumnDefs = loadColumnDefs(compConn, tableDef.schema, tableDef.tableName)
    val compColumnDefsMap = listToMap(compColumnDefs)

    // first iterate over the refColumns and look for missing from comp
    // or differences in comp
    val columnDiffs = ListBuffer[ColumnDiff]()
    refColumnDefs.foreach(columnDef =>
      if (!compColumnDefsMap.contains(columnDef.column_name)) {
        columnDiffs += ColumnDiff(columnDef.column_name, ColumnDiffType.MISSING_IN_COMP, None, None)
      } else {
        // column is in both, check for structural diff
        val compColumnDef = compColumnDefsMap.get(columnDef.column_name).get
        columnDiffs ++= compareColumnDefs(columnDef, compColumnDef)
      }
    )

    // now check for columns in compDB that are not in refDB
    compColumnDefs.foreach(columnDef =>
      if (!refColumnDefsMap.contains(columnDef.column_name)) {
        columnDiffs += ColumnDiff(columnDef.column_name, ColumnDiffType.MISSING_IN_REF, None, None)
      }
    )

    // create ordinality strings and check ordinality
    val refOrdinality = refColumnDefs.map(_.column_name).mkString(", ")
    val compOrdinality = compColumnDefs.map(_.column_name).mkString(", ")
    val ordinalityDiff = refOrdinality != compOrdinality

    // create primary key strings and check primary keys
    val refPrimaryKey = loadPrimaryKey(refConn, tableDef.schema, tableDef.tableName)
    val compPrimaryKey  = loadPrimaryKey(compConn, tableDef.schema, tableDef.tableName)
    val primaryKeyDiff = refPrimaryKey != compPrimaryKey


    if (columnDiffs.size > 0 || ordinalityDiff || primaryKeyDiff) {
      Some(
        TableDiff(tableDef,
          TableDiffType.STRUCTURE_DIFF,
          columnDiffs.toList,
          ordinalityDiff,
          Some(refOrdinality),
          Some(compOrdinality),
          primaryKeyDiff,
          Some(refPrimaryKey),
          Some(compPrimaryKey)
        )
      )
    } else {
      None
    }
  }

  /* Load tables from the information catalog */
  def loadTables(connection: Connection) : Set[TableDef] = {
    implicit val conn = connection

    val sql =
      "select * from information_schema.tables " +
      "where table_schema not in ('pg_catalog', 'information_schema') " +
      "order by table_name"

    val selectTables = anorm.SQL(sql)
    selectTables().map(row => {
      new TableDef(row[String]("table_name"), row[String]("table_schema"))
    }).toSet
  }


  /* Load the column definitions form the information catalog for a single table */
  def loadColumnDefs(conn: Connection, schema: String, tableName: String): List[ColumnDef] = {
    val sql =
      """
        |select * from information_schema.columns
        |where table_schema = {tableSchema} and table_name = {tableName}
        |order by ordinal_position
        |""".stripMargin

    val selectTables = anorm.SQL(sql).on('tableSchema ->schema, 'tableName -> tableName)
    implicit val impConn = conn
    selectTables().map(row => {
      val isNullable = row[String]("is_nullable") == "YES"
      val isIdentity = row[String]("is_identity") == "YES"
      new ColumnDef(
        row[String]("column_name"),
        row[String]("data_type"),
        row[Int]("ordinal_position"),
        isNullable,
        row[Option[Int]]("character_maximum_length"),
        row[Option[Int]]("numeric_precision"),
        row[Option[Int]]("numeric_precision_radix"),
        row[Option[Int]]("numeric_scale"),
        row[Option[Int]]("datetime_precision"),
        isIdentity,
        row[Option[String]]("column_default")
      )
    }).toList

  }

  def loadPrimaryKey(conn: Connection, schema:String, tableName : String) : String = {
    val sql =
      """
        |select pg_attribute.attname
        |from
        |	pg_index
        |	join pg_attribute on pg_attribute.attrelid = pg_index.indrelid and pg_attribute.attnum = any(pg_index.indkey)
        |where
        |	pg_index.indrelid = {tableSchemaAndName}::regclass
        |	and pg_index.indisprimary
      """.stripMargin

    val tableSchemaAndName   = schema + "." + tableName
    val selectTables = anorm.SQL(sql).on('tableSchemaAndName ->tableSchemaAndName)
    implicit val impConn = conn
    val keyColumns = selectTables().map(row => {
      row[String]("attname")
    }).toList

    // join thses somehow
    keyColumns.mkString(", ")

  }


  /* Compare the columns from a single table in 2 separate databases. and return a list of differences */
  def compareColumnDefs(refColumnDef : ColumnDef, compColumnDef : ColumnDef) : List[ColumnDiff] = {

    val columnDiffs = ListBuffer[ColumnDiff]()

    // check dataype is the same
    if (refColumnDef.dataTypeString != compColumnDef.dataTypeString) {
      columnDiffs += new ColumnDiff(
        refColumnDef.column_name ,
        ColumnDiffType.DATATYPE,
        Some(refColumnDef.dataTypeString),
        Some(compColumnDef.dataTypeString)
      )
    }

    // check nullability
    if (refColumnDef.is_nullable != compColumnDef.is_nullable) {
      columnDiffs += new ColumnDiff(
        refColumnDef.column_name ,
        ColumnDiffType.NULLABILITY,
        Some(if (refColumnDef.is_nullable) "NULLABLE" else "NOT NULLABLE"),
        Some(if (compColumnDef.is_nullable) "NULLABLE" else "NOT NULLABLE")
      )
    }

    // check default differences, column_default (if it exists) looks something like
    // 'daryl':character varying, we just want the 'daryl' part
    def parseDefault (columnDef : ColumnDef): String  = {
      columnDef.column_default.getOrElse("").split(":")(0)
    }
    def refDefault = parseDefault(refColumnDef)
    def compDefault =  parseDefault(compColumnDef)
    if (refDefault != compDefault)  {
      columnDiffs += new ColumnDiff(
        refColumnDef.column_name ,
        ColumnDiffType.DEFAULT,
        Some(refDefault),
        Some(compDefault)
      )
    }

    columnDiffs.toList
  }

}

