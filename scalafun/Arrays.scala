
def printarray (myArray : Array[Int]) :Unit = {
  myArray.foreach(x => println (x))
}
// this is a tuple
val whatIsThis = (1,2,3,4)

// Array
val array1 = Array(1,2,3,4)
println("list1")
// list1.foreach(x => println(x))
printarray(array1)

// filter with a function that returns a boolean
val list2 = array1.filter(x => x % 2 ==0)
println("list2")
list2.foreach(x => println(x))

// map by adding 1
val list3 = array1.map(x => x + 1)
println("list3")
list3.foreach(x => println(x))

// map to a string
val list4 = array1.map(x => "number " + x)
println("list3")
list4.foreach(x => println(x))




// val list2 = list1.for
