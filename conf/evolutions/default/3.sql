# --- !Ups

CREATE SEQUENCE league_id_seq;

CREATE TABLE league (
--  id integer NOT NULL UNIQUE  DEFAULT nextval('league_id_seq'),
  id varchar(10) UNIQUE,
  name varchar(255)
);

insert into league (id, name) values ('NHL', 'NHL');

# --- !Downs

DROP TABLE league;

DROP SEQUENCE league_id_seq;