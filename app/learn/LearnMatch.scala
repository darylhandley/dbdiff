package learn


/**
 * Created by darylhandley on 15-05-12.
 */

class NonCaseClass (name: String) {

}

case class CaseClass (name: String) {

}

object LearnMatch {

  def main(args: Array[String]) {

    // will not compile non case class can not be used in match
//    val nonCaseClass = new NonCaseClass("daryl")
//    println(nonCaseClass)
//    nonCaseClass match {
//      case NonCaseClass("daryl") => println("daryl")
//      case _ => println("Not daryl")
//    }

    val caseClass = CaseClass("daryl");
    println(caseClass)
    caseClass match {
      case CaseClass("daryl") => println("daryl")
      case _ => println("Not daryl")
    }

    println("hello world")
  }
}


