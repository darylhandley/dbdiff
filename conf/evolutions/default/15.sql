# --- !Ups
CREATE SEQUENCE ticket_id_seq;

CREATE TABLE ticket (
  id integer NOT NULL UNIQUE DEFAULT nextval('ticket_id_seq'),
  account_id integer not null REFERENCES account (id),
  status varchar(15) not null,
  wager_amount NUMERIC(10, 2) not null,
  potential_win NUMERIC(10, 2),
  actual_win numeric(10,2),
  created_date timestamp,
  resolved_date timestamp,
  win_status varchar(15)
);


# --- !Downs

DROP TABLE ticket;

DROP SEQUENCE ticket_id_seq;