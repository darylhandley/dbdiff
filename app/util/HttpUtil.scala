package util

import play.api.libs.ws.WS
import play.libs.WS.WSRequestHolder
import scala.util.{Failure, Success}
import concurrent.{ExecutionContext, Await}
import scala.concurrent.duration._
import ExecutionContext.Implicits.global
import play.api.Logger


/**
 *  Simple abstraction to
 */
object HttpUtil {

  /** Does a get on the url and returns the body as a string */
  def loadUrlAsString (url: String) : String = {

    // probabably a better way to do this and
    // - get rid of var for body
    // - get rid of await ?
    // - handle the failure case (throw exception ?)

    // this is a horrible hack because I don't really understand futures at this point
    // I am basically sleeping to get the future time to process
    // and thus I will then have the response by the time that the onComplete method is added (I think)
    // If we take this out then it does not seem to await the future as expected, not sure why but I think
    // it might be awaiting for the future to complete (to get the url), but not for the onComplete method to run and hence
    // there is still no body. Not removed the sleep and moved it before body return, this seems to give the onComplete
    // a chance to run
    val future = WS.url(url).get()
    // Thread sleep 1000
    var body = ""
    future onComplete {
      case Success(response) => {

        Logger.info("got response " + response.body)

        body = response.body
      }
    }

    Logger.info("waiting for response")
    Await.ready(future, 2000 millis)
    Logger.info("Finished waiting")


    Thread.sleep(100)
    return body
  }

}
