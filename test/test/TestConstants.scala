package test

/**
 * Created with IntelliJ IDEA.
 * User: daryl
 * Date: 2013-10-20
 * Time: 2:09 PM
 * To change this template use File | Settings | File Templates.
 */
object TestConstants {


  val COMPARISON_ID = 1

  val DATASTORE_ID_V123_REF =  1L
  val DATASTORE_ID_V123_NON_REF_2 =  2L
  val DATASTORE_ID_V123_NON_REF_3 =  3L

  val VERSION_ID_V123 =  1L
  val VERSION_ID_V124 =  2L
  val VERSION_ID_V125 =  3L

  val TICKET_ID_1 =  1


  val DBDIFF_TEST_COMPARE_REF = "dbdiff_testcompare_ref"
  val DBDIFF_TEST_COMPARE_COMP = "dbdiff_testcompare_comp"


}
