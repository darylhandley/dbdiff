# --- !Ups
CREATE SEQUENCE comparison_id_seq;

CREATE TABLE comparison (
  id integer NOT NULL UNIQUE DEFAULT nextval('comparison_id_seq'),
  reference_datastore_id integer not null references datastore(id),
  compare_datastore_id integer not null references datastore(id),
  status varchar(10),
  details varchar,
  created_time timestamp NOT NULL,
  start_time timestamp NULL,
  completed_time timestamp NULL
);


# --- !Downs

DROP TABLE comparison;

DROP SEQUENCE comparison_id_seq;