import akka.actor.Props
import scala.concurrent.ExecutionContext
import scala.concurrent.duration.Duration
import ExecutionContext.Implicits.global
import java.util.concurrent.TimeUnit
import jobs.{ComparisonRunner}
import org.squeryl.adapters.PostgreSqlAdapter
import org.squeryl.{Session, SessionFactory}
import play.api.db.DB
import play.api.{Application, GlobalSettings}
import play.libs.Akka
import play.Logger
import akka.actor.{Props, Actor}

/**
 * Created with IntelliJ IDEA.
 * User: daryl
 * Date: 2013-09-02
 * Time: 9:39 AM
 * To change this template use File | Settings | File Templates.
 */
object Global extends GlobalSettings {

  override def onStart(app: Application) {
    Logger.info("Starting up applicattion")

    SessionFactory.concreteFactory = Some(() =>
      Session.create(DB.getConnection()(app), new PostgreSqlAdapter)
    )

    // start up jobs
    // only schedule when not in tet mode
    play.api.Play.mode(app) match {
      case play.api.Mode.Test =>
      case _ => {
        scheduleJobs(app)
      }
    }
  }

  def scheduleJobs(app: Application) = {
    // all jobs disabled, reenable them later
//    Logger.info("Scheduling the reminder daemon")
//    val greeter = Akka.system.actorOf(Props(new Greeter()))
//    Akka.system().scheduler.schedule(
//      Duration.create(0, TimeUnit.MILLISECONDS),
//      Duration.create(60, TimeUnit.SECONDS),
//      greeter,
//      "greeter"
//    )
    // val reminderActor = Akka.system(app).actorOf(Props(new ReminderActor()))
    // Akka.system().scheduler.schedule(0 seconds, 5 minutes, reminderActor, "reminderDaemon")

    Logger.info("Scheduling the comparison runner  daemon")
    val comparisonRunner  = Akka.system.actorOf(Props(new ComparisonRunner()))
    Akka.system().scheduler.schedule(
      Duration.create(0, TimeUnit.MILLISECONDS),
      Duration.create(10, TimeUnit.SECONDS),
      comparisonRunner,
      "comparisonRnunner"
    )

  }

}
