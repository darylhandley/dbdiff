package util

import io.Source

/**
 *
 */
class FileUtil {

}

object FileUtil {

  /** Load file from relative path, currently hard coded, but we can change this later once we figure out how */
  def loadFile(filepath: String) : String = {
    val basepath = "/Users/darylhandley/dev/projects/betbaron/"
    val fullPath = basepath + filepath;
    Source.fromFile(fullPath, "ISO-8859-1" ).mkString
  }

}
