package fixture

import org.specs2.mutable.Specification
import play.api.test.Helpers._
import play.api.test.FakeApplication

/**
 * Specification for running backend tests (i.e db and integration tests)
 */
class BackendSpecification extends Specification {

  def freshDB[T](code: =>T) =
    running( FakeApplication()){
      FixtureUtil.resetDatabase
      code
    }

}
