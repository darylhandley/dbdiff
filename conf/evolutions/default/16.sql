# --- !Ups
CREATE SEQUENCE account_tran_id_seq;

CREATE TABLE account_tran (
  id integer NOT NULL UNIQUE DEFAULT nextval('account_tran_id_seq'),
  account_id integer not null REFERENCES account (id),
  tran_type varchar(15) not null,
  amount NUMERIC(10, 2) not null,
  balance NUMERIC(10, 2) not null
);


# --- !Downs

DROP TABLE account_tran;

DROP SEQUENCE account_tran_id_seq;