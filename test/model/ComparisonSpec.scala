package model

import fixture.BackendSpecification
import org.joda.time.DateTime
import test.TestConstants
import util.DateUtil

class ComparisonSpec extends BackendSpecification {
  
  "Comparison model" should {


    // test get by id
    "be retrieved by id" in freshDB {

      val comparison = Comparison.getById(1).get

      comparison.id must equalTo(1)
      comparison.referenceDatastoreId must equalTo(TestConstants.DATASTORE_ID_V123_REF)
      comparison.compareDatastoreId must equalTo(TestConstants.DATASTORE_ID_V123_NON_REF_2)
      comparison.status must equalTo("PASSED")
      comparison.details must equalTo("the details")
      // '2015-01-01 15:37:34', '2015-01-04 15:37:34', '2013-10-04 15:37:34' )",
      val createdTime = DateUtil.toTimestamp(new DateTime("2015-01-01T15:37:34"))
      comparison.createdTime must equalTo(createdTime)
      val startTime = DateUtil.toTimestamp(new DateTime("2015-01-01T16:37:34"))
      comparison.startTime.get must equalTo(startTime)
      val completedTime = DateUtil.toTimestamp(new DateTime("2015-01-01T17:37:34"))
      comparison.completedTime.get must equalTo(completedTime)

      comparison.referenceDatastore.id must equalTo(TestConstants.DATASTORE_ID_V123_REF)
      comparison.compareDatastore.id must equalTo(TestConstants.DATASTORE_ID_V123_NON_REF_2)

    }

    "be retrieved by id with null dates" in freshDB {

      val comparison = Comparison.getById(2).get

      comparison.id must equalTo(2)
      comparison.referenceDatastoreId must equalTo(TestConstants.DATASTORE_ID_V123_REF)
      comparison.compareDatastoreId must equalTo(TestConstants.DATASTORE_ID_V123_NON_REF_2)
      comparison.status must equalTo("PASSED")
      comparison.details must equalTo("the details")
      val createdTime = DateUtil.toTimestamp(new DateTime("2015-01-01T15:37:34"))
      comparison.createdTime must equalTo(createdTime)
      comparison.startTime must beNone
      comparison.completedTime must beNone

    }


    "insert" in freshDB {

      val createdTime = DateUtil.toTimestamp(new DateTime("2015-01-01T15:37:34"))
      val startTime = DateUtil.toTimestamp(new DateTime("2015-01-01T16:37:34"))
      val completedTime = DateUtil.toTimestamp(new DateTime("2015-01-01T17:37:34"))

      val comparison = Comparison(
        0L,
        TestConstants.DATASTORE_ID_V123_REF,
        TestConstants.DATASTORE_ID_V123_NON_REF_2,
        "PENDING",
        "my details",
        createdTime,
        Some(startTime),
        Some(completedTime)
      )

      val dbComparison = Comparison.insert(comparison)

      dbComparison.id must equalTo(100)

      dbComparison.referenceDatastoreId must equalTo(TestConstants.DATASTORE_ID_V123_REF)
      dbComparison.compareDatastoreId must equalTo(TestConstants.DATASTORE_ID_V123_NON_REF_2)
      dbComparison.status must equalTo("PENDING")
      dbComparison.details must equalTo("my details")
      comparison.createdTime must equalTo(createdTime)
      comparison.startTime.get must equalTo(startTime)
      comparison.completedTime.get must equalTo(completedTime)

    }

    "update" in freshDB {

      var comparison = Comparison.getById(1).get
      Comparison.update(comparison.copy(details = "New details"))

      comparison = Comparison.getById(1).get

      comparison.details must equalTo ("New details")

    }

    "delete" in freshDB {
      Comparison.delete(1L)
      val comparison = Comparison.getById(1)
      comparison must equalTo(None)
    }

    "retrieve all pending " in freshDB {

      // first check all are not pending
      var comparisons = Comparison.allPending()
      comparisons.size must equalTo(0)

      // update one of them to pending
      var comparison1 = Comparison.getById(TestConstants.COMPARISON_ID).get
      Comparison.update(comparison1.copy(status = "PENDING"))

      // now should have one item and it should be PENDING status
      comparisons = Comparison.allPending()
      comparisons.size must equalTo(1)
      comparisons.filter(c => c.status != "PENDING").size must equalTo(0)


    }


    
  }
  
}
