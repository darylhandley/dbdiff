'use strict';

/* Controllers */

angular.module('myApp.controllers', []).
  controller('MyCtrl1', [function() {
    // sample controller from seed to be removed
  }])
  .controller('BettingCtrl', ['$scope', '$http',  function($scope, $http) {
    // betting controller

    // for test
    $scope.sport = "NFL";

    // sample data for directive testing
    $scope.customer = {
      name: 'Daryl',
      address: '55 Hawthorne'
    };
    $scope.naomi = { name: 'Naomi', address: '1600 Amphitheatre' };
    $scope.igor = { name: 'Igor', address: '123 Somewhere' };

    $http.get('/events').success(function(data) {
        $scope.events = data;
    });


    $scope.addLineToTicket = function(lineId) {
        alert("line id is " + lineId);
    }


    $scope.testEvent = function() {
      alert("Test event is happening ");
    }

    $scope.customer = {
      name: 'Naomi',
      address: '1600 Amphitheatre'
    };



    }])
    .controller('MyCtrl2', ['$scope', function($scope) {
      // sample controller from seed to be removed
        $scope.sport = "NFL";
    }]);