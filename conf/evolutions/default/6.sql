# --- !Ups

CREATE SEQUENCE event_competitor_id_seq;
CREATE TABLE event_competitor (
  id integer NOT NULL UNIQUE DEFAULT nextval('event_competitor_id_seq') ,
  competitor_id integer REFERENCES competitor (id),
  event_id integer REFERENCES event  (id),
  num integer
);

insert into event_competitor (competitor_id, event_id, num) values (1, 1, 2);

# --- !Downs

DROP TABLE event_competitor;
DROP SEQUENCE event_competitor_id_seq;