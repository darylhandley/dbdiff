# Tasks schema
 
# --- !Ups

CREATE SEQUENCE player_id_seq;
CREATE TABLE player (
    id integer NOT NULL DEFAULT nextval('player_id_seq'),
    username varchar(255),
    email varchar(255), 
    password varchar(255)
);
 
# --- !Downs
 
DROP TABLE player;
DROP SEQUENCE player_id_seq;