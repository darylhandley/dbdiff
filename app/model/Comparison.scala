package model

import java.sql.Timestamp

import model.Database._
import org.squeryl.KeyedEntity
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.annotations.Column
import play.api.Logger

/**
 * Created by darylhandley on 15-04-22.
 */
case class Comparison (
  id : Long,
  @Column("reference_datastore_id") referenceDatastoreId : Long,
  @Column("compare_datastore_id") compareDatastoreId : Long,
  @Column("status") status : String,
  @Column("details") details : String,
  @Column("created_time") createdTime: Timestamp,
  @Column("start_time") startTime: Option[Timestamp],
  @Column("completed_time") completedTime: Option[Timestamp]
) extends KeyedEntity[Long] {

  lazy val referenceDatastore: Datastore = inTransaction {
    Database.datastoreToComparisonAsReference.right(this).toList(0)
  }

  lazy val compareDatastore: Datastore = inTransaction {
    Database.datastoreToComparisonAsCompare.right(this).toList(0)
  }

}


object ComparisonStatus {
  val PENDING = "PENDING"
  val PASSED = "PASSED"
  val FAILED = "FAILED"
  val RUNNING = "RUNNING"
}



object Comparison {
  def getById(id: Long): Option[Comparison] = inTransaction {
    comparisonTable.lookup(id)
  }

  def insert(comparison: Comparison): Comparison = inTransaction {
    comparisonTable.insert(comparison)
  }

  def delete(id: Long): Boolean = inTransaction {
    comparisonTable.delete(id)
  }

  def update(comparison: Comparison) {
    inTransaction {
      comparisonTable.update(comparison)
    }
  }

  def all()  = inTransaction {
    from(comparisonTable) {comparison => select(comparison)}.toList
  }

  def allPending(): List[Comparison] = inTransaction {
    from(comparisonTable) {
      comparison => where (comparison.status === "PENDING") select(comparison)
    }.toList
  }



}

