// FoldLeft.scala

// print the sum of all the elements 
var result = List(1,3,5).foldLeft(0) { _ + _ }
println(result) 

// print the sum of all the elements + 7 
result = List(1,3,5).foldLeft(7) { _ + _ }
println(result)

// same as above, but none shorthand version 
// print the sum of all the elements, on first iteration
// a is 0, then for every other element a is the result 
// of the previous iteration  
result = List(1,3,5).foldLeft(0) { (a,b) => a + b }
println(result)


// reverses a list and converts the integers to strings with an i
// it does this by taking an empty list and appending each element to the list
// since fold left is running from right to left this has the effect reversing the 
// list, in this case a is list and b is an element of the original list (an int) 
var result2  = List(1,3,5).foldLeft(List[String]()) { (a, b) => ("i" + b.toString) :: a }
println(result2)
// same as above using foldRight so this is not reversed, also we must switch up
// our a and b because of the way this is processed, in this case b is our initial element 
result2  = List(1,3,5).foldRight(List[String]()) { (a, b) => ("i" + a.toString) :: b }
println(result2)