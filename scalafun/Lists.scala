def printList (mylist : List[Int]) :Unit = {
  mylist.foreach(x => println (x))
}
// is this a tuple ?
val whatIsThis = (1,2,3,4)

// list
val list1 = List(1,2,3,4)
println("list1")
// list1.foreach(x => println(x))
printList(list1)

// filter with a function that returns a boolean
val list2 = list1.filter(x => x % 2 ==0)
println("list2")
list2.foreach(x => println(x))

// map by adding 1
val list3 = list1.map(x => x + 1)
println("list3")
list3.foreach(x => println(x))

// map to a string
val list4 = list1.map(x => "number " + x)
println("list3")
list4.foreach(x => println(x))
