'use strict';

/* Directives */


angular.module('myApp.directives', []).
  directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }])
  .directive('myCustomer', function() {
    return {
      // template: 'Name: {{customer.name}} Address: {{customer.address}}'
      restrict: 'E',
      templateUrl: '/assets/javascripts/angular-seed/app/partials/my-customer.html',
      scope: {
        customer: '=customer'
      }
    };
  })
  .directive('bbLinebutton', function($compile) {
    // can't get directive to work because it is not passing the event
    // back to the betting controller. Leave for now as we can just
    // repeat ourselves in the main code, but this guy has it working
    // http://stackoverflow.com/questions/16488769/ng-click-doesnt-work-within-the-template-of-a-directive
    // http://plnkr.co/edit/9XfXCpU6lhUOqD6nbVuQ?p=preview
    // this may also work and theoretically gives full access to parent scope
    // http://stackoverflow.com/questions/16866749/access-parent-scope-in-transcluded-directive
    return {
      // template: 'Name: {{customer.name}} Address: {{customer.address}}'
      restrict: 'E',
      templateUrl: '/assets/javascripts/angular-seed/app/partials/lineButton.html',
      scope: {
        line: '=line',
        fireEvent : '&'
      }
    };
  })
;

