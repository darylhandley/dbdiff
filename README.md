# dbdiff

dbdiff will compare db instances and report their differences. It is written in Scala using the Play Framework. 
 
# What it does
It will compare tables in the db and report if 
- a table is in one db and not the other 
- the tables ordinality is different (ordering of columns)
- tables have differing primary keys
- a column is missing from one table to the other 
- a column has a different datatype (including length)
- if a column is nullable in one db and non nullable in the other 
- if a column has a different default value

# What it doesn't do (yet)
- differences in foreign keys
- differences in  stored procedures
- differences in triggers 
- differences in constraints 
- anything else I haven't written above 

# Long term goals
- Proactively check databases across versions using a batch process and create nightly reports 
- Support stored procedures, constraints, triggers

# How to run it 

- clone repo
- download the Play Framework  and add it to your path (https://www.playframework.com/)  
- cd to cloned directory 
- activator run 

This will start it up at on http://localhost:9000/

- to start up on a different port use **activator "run 9001"**

