package compare

/**
 *  - CompareResult
 *    - List<TableDiff>
 *      - TableDiff
 *        - tableName : String (with schema ex: schema1.custoemer)
 *        - tableDiffType - TableDiffType: MISISNG_IN_REF_DS, MISSING_IN_COMP_DS, STRUCTURAL
 *      - columnDiffs : List<ColumnDiffs>
 *          - ColumnDiff
 *            - columnName : String
 *            - columnDiffType: ColumnDiffType (NULLABILITY, DATATYPE, DATATYPE, DEFAULT, PRIMARY_KEY, MISSING_IN_REF, MISSING_IN_COMP)
 *            - refDbValue : String
 *            - compareDbValue : String
 *      - ordinalityDiff : boolean
 *      - refTableOrdinality : Option[String]
 *      - compTableOrdinality : Option[String]
 *
 * - TODO:
 * - foreign key differences
 * - ordinal
 * - uniqueness constraints
 * - index differences
 * - sproc differences
 * - views
 * - seqences
 *
 *
 * =========================================================
 * Sample Data
 * =========================================================
 * The following differences were found
 *
 * schema.tableName1
 * ====================================
 * Table blah.blah exists in db1 but not in db2
 *
 * schema.tableName2
 * ====================================
 * Table schema.tableName2 exists in db2 but not in db1
 *
 * schema.customer
 * ====================================
 *
 * Column Differences
 * ------------------------------------
 * Column firstName exists in ref db but not in compare db.
 * Column first_name exists in ref db but not in compare db.
 * Column lastName type differs. Ref: varchar(100),  Compare: varchar(50)
 * Column lastName nullability differs. Ref: nullable ,  Compare: not nullable
 * Column lastName default value differs. Ref: 'Handley', Compare: no default
 * Column lastName ordinal value differs, Ref: 3, Compare: 4
 * Column lastName primary key differs, Ref: Primary Key, Compare: Not primary Key
 *
 * Ordinality differences
 * -------------------------------------
 * RefDb: id:1, firstName:2, middleName: 3, lastName:3
 * CompareDb: id:1, firstName:2, lastName:3, middleName: 4
 *
 * Foreign Key differences
 * -------------------------------------
 * need to find out more here
 * RefDb has FK_SOMETHING
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */
class CompareResult {

  var tableDiffs : List[TableDiff] = List[TableDiff]()

  def hasDifferences = tableDiffs.size > 0

}
