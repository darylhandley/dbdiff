'use strict';

/* Filters */

angular.module('myApp.filters', []).
  filter('interpolate', ['version', function(version) {
    return function(text) {
      return String(text).replace(/\%VERSION\%/mg, version);
    }
  }])
  .filter('displayLine', function() {
    return function(line) {


      if (typeof line === 'undefined') {
        return "N/D";
      }

      var retVal = ""
      if (line.lineType == "MONEY") {
        retVal = line.odds;
      } else if (line.lineType == "SPREAD") {
        retVal =  line.number + " at " + line.odds;
      } else if (line.lineType == "OVER") {
        retVal =  "O " + line.number + " at " + line.odds;
      } else if (line.lineType == "UNDER") {
        retVal =  "U " + line.number + " at " + line.odds;
      } else {
        retVal =  "N/A";
      }

      // retVal = line.id + " :: " + retVal;

      return retVal;



    }
  })
;
