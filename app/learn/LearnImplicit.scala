package learn

/**
 * Created by darylhandley on 15-05-13.
 */
class LearnImplicit {

}

object LearnImplicit {

  def doSomethingImplicit (greeting : String) (implicit name : String) : Unit =  {

    println(greeting + " " + name)

  }


  def main(args: Array[String]): Unit = {

    val string1 = "Hello"
    implicit var string2 = "Daryl"
    doSomethingImplicit(string1)

    // uncomment this will cause "ambiguous" compile error because you can only have one implicit of each type
    // at runtime (i.e. only one impllicit string in this case)
    // implicit val name2 = "Steve"

    string2 = "Steve"
    doSomethingImplicit(string1)

    // pass implicit value explicitly
    doSomethingImplicit(string1)("Greg")


  }
}
