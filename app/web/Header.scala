package web

import play.api.mvc.Request

case class Header(menu: Seq[MenuItem], user: Option[String])

case class MenuItem(url: String, name: String)

trait ProvidesHeader {
  implicit def header[A](implicit request: Request[A]) : Header = {
    val menu = Seq(MenuItem("/home", "Home"),
      MenuItem("/about", "About"),
      MenuItem("/contact", "Contact"))



    // can use this later if we ever integrate with ldap
    val user = Option("Daryl")

    Header(menu, user)
  }
}