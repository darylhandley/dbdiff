# --- !Ups
delete from datastore;
alter  TABLE datastore  add column version_id integer not null references version(id);


# --- !Downs
alter table datastore drop column version_id;
