package learn

/**
 * Created by darylhandley on 15-05-14.
 */
class LearnIfElse {



}

object LearnIfElse {

  def main(args: Array[String]): Unit = {

    val myString = "Blah"
    val myOtherString =
      if (myString.contains("a")) {
        if (myString.contains("B")) "contains a and B" else "contains A"
      } else {
        "doesn't contain a or B"
      }
    println(myOtherString)
  }

}
