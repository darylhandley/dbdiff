import concurrent.{Await, ExecutionContext}
import play.api.libs.ws.WS
import play.libs.WS.WSRequestHolder
import scala.util.{Failure, Success}
import ExecutionContext.Implicits.global
import scala.concurrent.duration._


val future = WS.url("http://httpbin.org/get").get()

var body = ""
future onComplete {
  case Success(response) => {
    println("Http request completed")
    body = response.body
  }
  case Failure(t) => println("An error has occured: " + t.getMessage)
}

Await.ready(future, 2000 millis)
println ("Got response body : " + body)


