package util

import org.specs2.mutable._
import play.Logger
import org.joda.time.DateTime
import java.sql.Timestamp

/**
 */
class DateUtilSpec extends Specification {

  "Date Util" should {

    "convert datetime to timestamp" in {

      val result = DateUtil.toTimestamp(new DateTime(2000,1,1,11,59,59))
      val expected = new Timestamp(new DateTime(2000,1,1,11,59,59).toDate.getTime)

      result must equalTo(expected)

    }

  }

}
