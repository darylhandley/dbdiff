# --- !Ups
CREATE SEQUENCE account_id_seq;

ALTER TABLE player ADD CONSTRAINT player_id_uq UNIQUE (id);

CREATE TABLE account (
  id integer NOT NULL UNIQUE DEFAULT nextval('account_id_seq'),
  player_id integer not null REFERENCES player (id),
  pool_id integer not null,
  balance NUMERIC(10, 2) not null
);


# --- !Downs

DROP TABLE account;

DROP SEQUENCE account_id_seq;