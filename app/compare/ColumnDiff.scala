package compare

import compare.ColumnDiffType.ColumnDiffType

/*
*          - ColumnDiff
*            - columnName : String
*            - columnDiffType: ColumnDiffType (NULLABILITY, DATATYPE, DATATYPE, DEFAULT, PRIMARY_KEY)
*            - refDbValue : String
*            - compareDbValue : String
*/

case class ColumnDiff (
  columnName: String,
  columnDiffType: ColumnDiffType,
  refDbValue: Option[String],
  compDbValue: Option[String]
)
