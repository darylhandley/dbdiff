# --- !Ups

CREATE SEQUENCE event_id_seq;
CREATE TABLE event (
  id integer NOT NULL UNIQUE DEFAULT nextval('event_id_seq') ,
  league_id varchar(10)  REFERENCES league (id)
);

insert into event (league_id) values ('NHL');

# --- !Downs

DROP TABLE event;
DROP SEQUENCE event_id_seq;