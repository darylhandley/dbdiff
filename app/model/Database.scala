package model

import org.squeryl.{Table, Schema}
import org.squeryl.PrimitiveTypeMode._

class Database {

}

object Database extends Schema {

  // version
  val versionTable : Table[Version] =
    table[Version]("version")

  on(versionTable)(p => declare(
    p.id is(autoIncremented("version_id_seq"))
  ))

  // datastore
  val datastoreTable : Table[Datastore] =
    table[Datastore]("datastore")

  on(datastoreTable)(p => declare(
    p.id is(autoIncremented("datastore_id_seq"))
  ))

  val versionToDatastore =
    oneToManyRelation(versionTable, datastoreTable).
      via((version, datastore) => version.id === datastore.versionId)


  // comparison
  val comparisonTable : Table[Comparison] =
    table[Comparison]("comparison")

  on(comparisonTable)(p => declare(
    p.id is(autoIncremented("comparison_id_seq"))
  ))

  val datastoreToComparisonAsReference =
    oneToManyRelation(datastoreTable, comparisonTable).
      via((datastore, comparison) => datastore.id === comparison.referenceDatastoreId)

  val datastoreToComparisonAsCompare =
    oneToManyRelation(datastoreTable, comparisonTable).
      via((datastore, comparison) => datastore.id === comparison.compareDatastoreId)



}