# --- !Ups
ALTER TABLE event
ADD COLUMN status varchar NOT NULL DEFAULT 'PENDING';


# --- !Downs
ALTER TABLE event
drop COLUMN status;