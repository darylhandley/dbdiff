package compare

import org.specs2.mutable._

class CompareResultFormatterSpec extends Specification {

    "Column Def" should {

        "format dataType correctly no size info" in {
            val colDef = createColumnDef
            colDef.dataTypeString must equalTo("myDataType")
        }

        "format dataType correctly with character_maximum_length" in {
            val colDef = createColumnDef.copy(
                character_maximum_length = Some(10)
            )
            colDef.dataTypeString must equalTo("myDataType (10)")
        }

      "format dataType correctly with numeric values (10)" in {
        val colDef = createColumnDef.copy(
          numeric_precision = Some(10)
        )
        colDef.dataTypeString must equalTo("myDataType (10)")
      }

        "format dataType correctly with numeric values (10,2)" in {
            val colDef = createColumnDef.copy(
                numeric_precision = Some(10),
                numeric_scale = Some(2)

            )
            colDef.dataTypeString must equalTo("myDataType (10,2)")
        }

    }

    def createColumnDef() : ColumnDef = {
        ColumnDef(
        "myColumnName" ,
        "myDataType",
        1,
        true,
        None,
        None,
        None,
        None,
        None,
        false,
        None
    )

    }
}
