# --- !Ups
CREATE SEQUENCE line_selection_id_seq;

CREATE TABLE line_selection (
  id integer NOT NULL UNIQUE DEFAULT nextval('line_selection_id_seq'),
  ticket_id integer not null REFERENCES ticket (id),
  line_id integer not null references line (id),
  odds NUMERIC(10, 2) not null
);


# --- !Downs

DROP TABLE line_selection;

DROP SEQUENCE line_selection_id_seq;