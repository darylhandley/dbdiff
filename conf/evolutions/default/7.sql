# --- !Ups

CREATE SEQUENCE line_id_seq;
CREATE TABLE line (
  id integer NOT NULL UNIQUE DEFAULT nextval('line_id_seq') ,
  event_competitor_id integer REFERENCES event_competitor (id),
  event_id integer REFERENCES event (id),
  odds integer not null default 0
);

insert into line (event_competitor_id, event_id, odds) values (1, 1, 100);

# --- !Downs

DROP TABLE line;
DROP SEQUENCE line_id_seq;