package util

import javax.sql.DataSource

import model.Datastore
import org.apache.commons.dbcp.BasicDataSource

object DataSourceUtil {

  def createDataSourceFromDataStore(datastore : Datastore) : BasicDataSource = {
    val ds = new BasicDataSource
    ds.setDriverClassName("org.postgresql.Driver")
    ds.setUsername(datastore.username)
    ds.setPassword(datastore.password)
    ds.setUrl("jdbc:postgresql://" + datastore.host + ":" + datastore.port+ "/" + datastore.dbName)
    ds


  }


}
