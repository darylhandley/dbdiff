package web

import model.{Datastore, Version}

/**
 * Created by darylhandley on 15-05-06.
 */
object OptionUtil {

  def versionOptions = {
    // Version.all()
    // Seq("01 hour","02 hour","03 hour","Never end")
    // Seq(("01 hour", "01 hour") , ("02 hour", "02 hour"))
    // Version.all().map(version => version.id.toString -> version.name)
    Version.all().map(version => Tuple2(version.id.toString, version.name))
  }

  def datastoreOptions  = {
    Datastore.all().map(datastore => Tuple2(datastore.id.toString, datastore.name))
  }

}
