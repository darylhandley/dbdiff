import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

    val appName         = "DbDiffWeb"
    val appVersion      = "1.0-SNAPSHOT"

    val appDependencies = Seq(
      // Add your project dependencies here,
      jdbc,
      javaCore,
      javaJdbc,
      anorm,
      javaJpa,
      filters,
      javaEbean,
      cache,
      "postgresql" % "postgresql" % "8.4-702.jdbc4",
      "org.squeryl" %% "squeryl" % "0.9.5-6",
      "commons-dbcp" % "commons-dbcp" % "1.4"

      // "org.scalaj" % "scalaj-http_2.8.0" % "0.3.2"
    )


    val main = play.Project(appName, appVersion, appDependencies).settings(
      // Add your own project settings here
    )

}
