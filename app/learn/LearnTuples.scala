package learn

/**
 * Created by darylhandley on 15-05-14.
 */
class LearnTuples {

}

object LearnTuples {

  def main(args: Array[String]): Unit = {

    // -> syntax
    // can only be used for tuple2
    val firstAndLast = "Daryl" -> "Handley"
    println(firstAndLast._1 + " " + firstAndLast._2)

    // doesn't work
    // val firstAndLastAndAge : Tuple2[String, String, Int] = "Daryl" -> "Handley" -> 43

    // need this
    val firstAndLastAndAge : Tuple3[String, String, Int] = ("Daryl", "Handley", 43)
    println(firstAndLastAndAge.toString())
    println(firstAndLastAndAge)

    // or this (alternate tuple sig)
    val firstAndLastAndAge2 : (String, String, Int) = ("Daryl", "Handley", 43)

    // iterate over tuple
    firstAndLastAndAge2.productIterator.foreach(println)

    // -> syntax is good for creatting maps
    val map = Map(1->"a", 2->"b")
    println(map)
    println(map.get(1).get)
    println(map.get(2).get)
    // println(map.get(3))


    // turn a list of tuples into a map (useful for hashing objects by a member ex: hash customers by customerId
    // first create tuples of customer id and customer and then convert to map)
    val myCustomerList = List((1, "Daryl"), (2, "Elise") , (3 , "Evan"))
    // or val myCustomerList = List(1 ->  "Daryl", 2 -> "Elise", 3 -> "Evan")
    val customerMap = myCustomerList.toMap
    println(customerMap(1))
    println(customerMap(2))
    println(customerMap(3))





  }

}
