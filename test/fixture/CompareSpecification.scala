package fixture

import org.specs2.mutable.Specification
import play.api.test.FakeApplication
import play.api.test.Helpers._

/**
 * Specification for running comparison tests.
 */
class CompareSpecification extends Specification {

  def freshDB[T](code: =>T) =
    running( FakeApplication()){
      FixtureUtil.createComparisonDatabases
      code
    }

}
