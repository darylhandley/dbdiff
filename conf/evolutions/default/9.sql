# --- !Ups
delete from line;

ALTER TABLE "public"."line"
ADD COLUMN "line_type" varchar(255) NOT NULL;


# --- !Downs
ALTER TABLE line
  drop COLUMN line_type;