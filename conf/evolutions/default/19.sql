# --- !Ups
ALTER TABLE account_tran
ADD COLUMN ticket_id integer null references ticket(id);


# --- !Downs
ALTER TABLE account_tran
drop COLUMN ticket_id;