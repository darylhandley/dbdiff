package compare

import org.h2.server.web.DbSchema

/**
 * Table definition. Holds the defition of a table as read from information_schema.tables
 */
case class TableDef (tableName : String , schema: String) {

  def fullName = schema + "." + tableName

}
