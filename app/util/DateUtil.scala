package util

import java.sql.Timestamp
import org.joda.time.DateTime


/**
 *  Utility for converting dates between different formats.
 */
object DateUtil {

  /** convert the joda DateTime to a java.sql.TimeStamp  */
  def toTimestamp (datetime: DateTime) = new Timestamp(datetime.toDate.getTime)

}
