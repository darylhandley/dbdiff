package compare

class CompareResultFormatter {

}

object CompareResultFormatter {
  def sepSize = 100

  def format(compareResult: CompareResult) : String = {
    var sb = ""
    compareResult.tableDiffs.foreach { tableDiff =>
      sb += "=" * sepSize + "\n"
      sb += "Differences for table " + tableDiff.tableDef.fullName + "\n"
      sb += "-" * sepSize + "\n"
      if (tableDiff.tableDiffType == TableDiffType.IN_REF_ONLY) {
        sb += "Table is only in reference db \n"
      } else if (tableDiff.tableDiffType == TableDiffType.IN_COMPARE_ONLY) {
        sb += "Table is only in compare db \n"
      }
      if (tableDiff.ordinalityDiff) {
        sb += "Ordinality difference \n"
        sb += "Reference : " + tableDiff.refTableOrdinality.get + "\n"
        sb += "Compare   : " + tableDiff.compTableOrdinality.get + "\n"
        sb += "-" * sepSize + "\n"
      }

      if (tableDiff.primaryKeyDiff) {
        sb += "Primary Key difference \n"
        sb += "Reference : " + tableDiff.refPrimaryKey.get + "\n"
        sb += "Compare   : " + tableDiff.compPrimaryKey.get + "\n"
        sb += "-" * sepSize + "\n"
      }

      if (tableDiff.columnDiffs.size > 0) {
        tableDiff.columnDiffs.foreach {columnDiff =>
          sb += createColumnDetails(columnDiff)
          sb += "-" * sepSize + "\n"
        }
      }

    }
    sb
  }

  private def createColumnDetails(columnDiff : ColumnDiff): String = {
    var sb = "Column \"" + columnDiff.columnName + "\" differs \n"
    if (columnDiff.columnDiffType == ColumnDiffType.DATATYPE) {
      sb += "Data type differs ref = \"" + columnDiff.refDbValue.get +
        "\", comp = \"" + columnDiff.compDbValue.get + "\"\n"
    }

    if (columnDiff.columnDiffType == ColumnDiffType.MISSING_IN_COMP) {
      sb += "Column exists in reference db but is missing in compare db" + "\n"
    }

    if (columnDiff.columnDiffType == ColumnDiffType.MISSING_IN_REF) {
      sb += "Column exists in compare db but is missing in reference db" + "\n"
    }

    if (columnDiff.columnDiffType == ColumnDiffType.NULLABILITY) {
      sb += "Nullability differs. Reference db is " + columnDiff.refDbValue.get +
        " but compare db is " + columnDiff.compDbValue.get + "\n"
    }

    if (columnDiff.columnDiffType == ColumnDiffType.DEFAULT) {
      sb += "Column defaults differs. Reference db is " + columnDiff.refDbValue.get +
        " but compare db is " + columnDiff.compDbValue.get + "\n"
    }

    sb
  }

}
