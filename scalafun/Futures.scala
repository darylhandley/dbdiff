import java.lang.String
import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.Predef.String
import scala.util.{Success, Failure}
import scala.concurrent.duration._

val getMyName: Future[String] = future {
  Thread sleep 1000
  "daryl"
}

getMyName onComplete {
  case Success(myName) => println("My Name is " + myName)
  case Failure(t) => println("An error has occured: " + t.getMessage)
}


getMyName onSuccess {
  case myName => println("My Name is still " + myName)

}


println("Waiting for callbacks to trigger")
Await.ready(getMyName, 3000 millis)


