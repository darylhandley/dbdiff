package model

import fixture.BackendSpecification

class VersionSpec extends BackendSpecification {
  
  "Version model" should {


    // test get by id
    "be retrieved by id" in freshDB {

      val version = Version.getById(1).get

      version.id must equalTo(1)
      version.name must equalTo("1.2.3")

    }


    "insert" in freshDB {

      val version = Version(
        0L,
        "2.3.4"
      )

      val dbVersion = Version.insert(version)

      dbVersion.id must equalTo(100)
      dbVersion.name must equalTo("2.3.4")

    }

    "update" in freshDB {

      var version = Version.getById(1).get
      Version.update(version.copy(name = "New Version Name"))

      version = Version.getById(1).get

      version.name  must equalTo ("New Version Name")

    }

    "delete" in freshDB {
      // insert new
      val version = Version(
        0L,
        "2.3.5"
      )

      val dbVersion = Version.insert(version)
      Version.delete(dbVersion.id)

      val versionOption = Version.getById(dbVersion.id)
      versionOption must equalTo(None)
    }
    
  }
  
}
