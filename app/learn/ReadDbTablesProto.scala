package learn

import java.sql.Connection
import javax.sql.DataSource

import anorm.SqlRow
import org.apache.commons.dbcp.BasicDataSource


object ReadDbTablesProto {

  def main( args : Array[String]): Unit = {

    println( "*" * 20 + " starting up")

    val dataSource: DataSource = {
      val ds = new BasicDataSource
      ds.setDriverClassName("org.postgresql.Driver")
      ds.setUsername("xm_mustafar")
      ds.setPassword("")
      ds.setMaxActive(20);
      ds.setMaxIdle(10);
      ds.setInitialSize(10);
      ds.setUrl("jdbc:postgresql://localhost/xm_mustafar")
      ds
    }

    // test the data source validity
    implicit val conn = dataSource.getConnection()




    val selectTables = anorm.SQL("select * from information_schema.tables where table_schema not in ('pg_catalog', 'information_schema') order by table_name")
    selectTables().foreach(row => {
        println(row[String]("table_name"))
      }
    )

    conn.close()

    println( "*" * 20 + " done")
  }



}
