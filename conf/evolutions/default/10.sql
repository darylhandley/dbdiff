# --- !Ups
ALTER TABLE line
ADD COLUMN number varchar(10) NULL;


# --- !Downs
ALTER TABLE line
drop COLUMN number;