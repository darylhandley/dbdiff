package compare

import javax.sql.DataSource

import fixture.{CompareSpecification, BackendSpecification}
import model.{Datastore}
import org.apache.commons.dbcp.BasicDataSource
import org.specs2.mutable.Specification
import org.squeryl.annotations._
import play.api.test.FakeApplication
import play.api.test.Helpers._
import anorm.SQL
import test.TestConstants
import compare.TableDiffType._
import util.DataSourceUtil

/**
 * Created by darylhandley on 15-05-19.
 */
class PostgresCompareSpec extends CompareSpecification {

  val refDatastore = Datastore(
    0,
    "Test Compare 01",
    "localhost",
    5432,
    "darylhandley",
    "",
    TestConstants.DBDIFF_TEST_COMPARE_REF,
    0,
    false
  )
  val compareDatastore = Datastore(
    0,
    "Test Compare 02",
    "localhost",
    5432,
    "darylhandley",
    "",
    TestConstants.DBDIFF_TEST_COMPARE_COMP,
    0,
    false
  )


  "return no diffs on default db setup (identical dbs) " in freshDB {
    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs
    tableDiffs.size must equalTo(0)
    compareResult.hasDifferences must beFalse
  }

  "return diff on extra table in compare db " in freshDB {

    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    val tableDiff = tableDiffs(0)
    tableDiff.tableDiffType must equalTo(IN_COMPARE_ONLY)
    tableDiff.columnDiffs.size must equalTo(0)
    tableDiff.compTableOrdinality must beNone
    tableDiff.ordinalityDiff must beFalse
    tableDiff.tableDef.fullName must equalTo("public.newtable01")
    tableDiff.refTableOrdinality must beNone
    compareResult.hasDifferences must beTrue

  }

  "return diff on extra table in ref db " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, primary key(id))")


    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    val tableDiff = tableDiffs(0)
    tableDiff.tableDiffType must equalTo(IN_REF_ONLY)
    tableDiff.columnDiffs.size must equalTo(0)
    tableDiff.compTableOrdinality must beNone
    tableDiff.ordinalityDiff must beFalse
    tableDiff.tableDef.fullName must equalTo("public.newtable01")
    tableDiff.refTableOrdinality must beNone

    compareResult.hasDifferences must beTrue

  }

  "show missing table differences for table in different schema  " in freshDB {

    executeWithDataStore(refDatastore, "CREATE SCHEMA aaa")
    executeWithDataStore(refDatastore, "create table aaa.newtable01 (id bigint, primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, primary key(id))")


    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(2)

    // this will be first since we return our results alphabetically by schema.tableName
    var tableDiff = tableDiffs(0)
    tableDiff.tableDiffType must equalTo(IN_REF_ONLY)
    tableDiff.columnDiffs.size must equalTo(0)
    tableDiff.compTableOrdinality must beNone
    tableDiff.ordinalityDiff must beFalse
    tableDiff.tableDef.fullName must equalTo("aaa.newtable01")
    tableDiff.refTableOrdinality must beNone

    tableDiff = tableDiffs(1)
    tableDiff.tableDiffType must equalTo(IN_COMPARE_ONLY)
    tableDiff.columnDiffs.size must equalTo(0)
    tableDiff.compTableOrdinality must beNone
    tableDiff.ordinalityDiff must beFalse
    tableDiff.tableDef.fullName must equalTo("public.newtable01")
    tableDiff.refTableOrdinality must beNone

    compareResult.hasDifferences must beTrue

  }

  "show colum differences for missing column in compare db  " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, first_name varchar, primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)

    var tableDiff = tableDiffs(0)
    tableDiff.tableDiffType must equalTo(STRUCTURE_DIFF)
    tableDiff.columnDiffs.size must equalTo(1)
    tableDiff.ordinalityDiff must beTrue
    tableDiff.tableDef.fullName must equalTo("public.newtable01")
    tableDiff.refTableOrdinality must equalTo(Some("id"))
    tableDiff.compTableOrdinality must equalTo(Some("id, first_name"))

    val columnDiff = tableDiff.columnDiffs(0)
    columnDiff.columnDiffType must equalTo(ColumnDiffType.MISSING_IN_REF)
    columnDiff.columnName must equalTo("first_name")
    columnDiff.compDbValue must beNone
    columnDiff.refDbValue must beNone

    compareResult.hasDifferences must beTrue

  }

  "show colum differences for missing column in comp db  " in freshDB {


    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, first_name varchar, primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)

    var tableDiff = tableDiffs(0)
    tableDiff.tableDiffType must equalTo(STRUCTURE_DIFF)
    tableDiff.columnDiffs.size must equalTo(1)
    tableDiff.ordinalityDiff must beTrue
    tableDiff.tableDef.fullName must equalTo("public.newtable01")
    tableDiff.refTableOrdinality must equalTo(Some("id, first_name"))
    tableDiff.compTableOrdinality must equalTo(Some("id"))

    val columnDiff = tableDiff.columnDiffs(0)
    columnDiff.columnDiffType must equalTo(ColumnDiffType.MISSING_IN_COMP)
    columnDiff.columnName must equalTo("first_name")
    columnDiff.compDbValue must beNone
    columnDiff.refDbValue must beNone

    compareResult.hasDifferences must beTrue

  }

  "show colum differences for data type  " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, first_name varchar(10), primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, first_name char(10), primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    var tableDiff = tableDiffs(0)
    tableDiffs(0).tableDiffType must equalTo(STRUCTURE_DIFF)

    val columnDiff = tableDiff.columnDiffs(0)
    columnDiff.columnDiffType must equalTo(ColumnDiffType.DATATYPE)
    columnDiff.columnName must equalTo("first_name")
    columnDiff.refDbValue.get must equalTo("character varying (10)")
    columnDiff.compDbValue.get must equalTo("character (10)")

    compareResult.hasDifferences must beTrue

  }

  "show colum differences character size " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, first_name char(11), primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, first_name char(10), primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    var tableDiff = tableDiffs(0)
    tableDiffs(0).tableDiffType must equalTo(STRUCTURE_DIFF)

    val columnDiff = tableDiff.columnDiffs(0)
    columnDiff.columnDiffType must equalTo(ColumnDiffType.DATATYPE)
    columnDiff.columnName must equalTo("first_name")
    columnDiff.refDbValue.get must equalTo("character (11)")
    columnDiff.compDbValue.get must equalTo("character (10)")

    compareResult.hasDifferences must beTrue

  }

  "show colum differences for numeric size  " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, first_name numeric (10,2), primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, first_name numeric (11,2), primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    var tableDiff = tableDiffs(0)
    tableDiffs(0).tableDiffType must equalTo(STRUCTURE_DIFF)

    val columnDiff = tableDiff.columnDiffs(0)
    columnDiff.columnDiffType must equalTo(ColumnDiffType.DATATYPE)
    columnDiff.columnName must equalTo("first_name")
    columnDiff.refDbValue.get must equalTo("numeric (10,2)")
    columnDiff.compDbValue.get must equalTo("numeric (11,2)")

    compareResult.hasDifferences must beTrue

  }

  "show colum differences for timestamp with and without timezone  " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, mytz timestamp with time zone, primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, mytz timestamp without time zone, primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    var tableDiff = tableDiffs(0)
    tableDiffs(0).tableDiffType must equalTo(STRUCTURE_DIFF)

    val columnDiff = tableDiff.columnDiffs(0)
    columnDiff.columnDiffType must equalTo(ColumnDiffType.DATATYPE)
    columnDiff.columnName must equalTo("mytz")
    columnDiff.refDbValue.get must equalTo("timestamp with time zone")
    columnDiff.compDbValue.get must equalTo("timestamp without time zone")

    compareResult.hasDifferences must beTrue

  }

  "show colum differences for timestamp and time " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, mytime timestamp, primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, mytime time, primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    var tableDiff = tableDiffs(0)
    tableDiffs(0).tableDiffType must equalTo(STRUCTURE_DIFF)

    val columnDiff = tableDiff.columnDiffs(0)
    columnDiff.columnDiffType must equalTo(ColumnDiffType.DATATYPE)
    columnDiff.columnName must equalTo("mytime")
    columnDiff.refDbValue.get must equalTo("timestamp without time zone")
    columnDiff.compDbValue.get must equalTo("time without time zone")

    compareResult.hasDifferences must beTrue

  }

  "show colum differences for timestamp and date  " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, mytime timestamp, primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, mytime date, primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    var tableDiff = tableDiffs(0)
    tableDiffs(0).tableDiffType must equalTo(STRUCTURE_DIFF)

    val columnDiff = tableDiff.columnDiffs(0)
    columnDiff.columnDiffType must equalTo(ColumnDiffType.DATATYPE)
    columnDiff.columnName must equalTo("mytime")
    columnDiff.refDbValue.get must equalTo("timestamp without time zone")
    columnDiff.compDbValue.get must equalTo("date")

    compareResult.hasDifferences must beTrue

  }

  "show colum differences for nullability " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, first_name varchar(10) not null, primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, first_name varchar(10) null, primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    var tableDiff = tableDiffs(0)
    tableDiffs(0).tableDiffType must equalTo(STRUCTURE_DIFF)

    val columnDiff = tableDiff.columnDiffs(0)
    columnDiff.columnDiffType must equalTo(ColumnDiffType.NULLABILITY)
    columnDiff.columnName must equalTo("first_name")
    columnDiff.refDbValue.get must equalTo("NOT NULLABLE")
    columnDiff.compDbValue.get must equalTo("NULLABLE")

    compareResult.hasDifferences must beTrue

  }

  "show colum differences for nullability reverse" in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, first_name varchar(10) null, primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, first_name varchar(10) not null, primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    var tableDiff = tableDiffs(0)
    tableDiffs(0).tableDiffType must equalTo(STRUCTURE_DIFF)

    val columnDiff = tableDiff.columnDiffs(0)
    columnDiff.columnDiffType must equalTo(ColumnDiffType.NULLABILITY)
    columnDiff.columnName must equalTo("first_name")
    columnDiff.refDbValue.get must equalTo("NULLABLE")
    columnDiff.compDbValue.get must equalTo("NOT NULLABLE")

    compareResult.hasDifferences must beTrue

  }

  // DEFAULT
  "show colum differences for default values when ref has a default " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, first_name varchar(10) default 'daryl' , primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, first_name varchar(10), primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    var tableDiff = tableDiffs(0)
    tableDiffs(0).tableDiffType must equalTo(STRUCTURE_DIFF)

    val columnDiff = tableDiff.columnDiffs(0)
    columnDiff.columnDiffType must equalTo(ColumnDiffType.DEFAULT)
    columnDiff.columnName must equalTo("first_name")
    columnDiff.refDbValue.get must equalTo("'daryl'")
    columnDiff.compDbValue.get must equalTo("")

    compareResult.hasDifferences must beTrue

  }

  "show colum differences for default values when comp has a default " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, first_name varchar(10) , primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, first_name varchar(10) default 'daryl', primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    var tableDiff = tableDiffs(0)
    tableDiffs(0).tableDiffType must equalTo(STRUCTURE_DIFF)
    tableDiff.ordinalityDiff must beFalse

    val columnDiff = tableDiff.columnDiffs(0)
    columnDiff.columnDiffType must equalTo(ColumnDiffType.DEFAULT)
    columnDiff.columnName must equalTo("first_name")
    columnDiff.refDbValue.get must equalTo("")
    columnDiff.compDbValue.get must equalTo("'daryl'")

    compareResult.hasDifferences must beTrue

  }

  "show colum differences for default values when both have a default " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, first_name varchar(10) default 'milli', primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, first_name varchar(10) default 'vanilli', primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    var tableDiff = tableDiffs(0)
    tableDiffs(0).tableDiffType must equalTo(STRUCTURE_DIFF)
    tableDiff.ordinalityDiff must beFalse

    val columnDiff = tableDiff.columnDiffs(0)
    columnDiff.columnDiffType must equalTo(ColumnDiffType.DEFAULT)
    columnDiff.columnName must equalTo("first_name")
    columnDiff.refDbValue.get must equalTo("'milli'")
    columnDiff.compDbValue.get must equalTo("'vanilli'")

    compareResult.hasDifferences must beTrue

  }

  "show colum differences for numeric default " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint default 10, first_name varchar(10) , primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, first_name varchar(10), primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    var tableDiff = tableDiffs(0)
    tableDiffs(0).tableDiffType must equalTo(STRUCTURE_DIFF)
    tableDiff.ordinalityDiff must beFalse

    val columnDiff = tableDiff.columnDiffs(0)
    columnDiff.columnDiffType must equalTo(ColumnDiffType.DEFAULT)
    columnDiff.columnName must equalTo("id")
    columnDiff.refDbValue.get must equalTo("10")
    columnDiff.compDbValue.get must equalTo("")

    compareResult.hasDifferences must beTrue

  }

  "show ordinality differences  " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, last_name varchar(10), first_name varchar(10) , primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, first_name varchar(10), last_name varchar(10), primary key(id))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    var tableDiff = tableDiffs(0)
    tableDiffs(0).tableDiffType must equalTo(STRUCTURE_DIFF)

    tableDiff.columnDiffs.size must equalTo(0)
    tableDiff.ordinalityDiff must beTrue
    tableDiff.refTableOrdinality must equalTo(Some("id, last_name, first_name"))
    tableDiff.compTableOrdinality must equalTo(Some("id, first_name, last_name"))

    compareResult.hasDifferences must beTrue

  }

  "show primay key differences  " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, last_name varchar(10), first_name varchar(10) , primary key(id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, last_name varchar(10), first_name varchar(10) , primary key(id, first_name))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(1)
    var tableDiff = tableDiffs(0)
    tableDiff.tableDiffType must equalTo(STRUCTURE_DIFF)
    tableDiff.primaryKeyDiff must beTrue
    tableDiff.refPrimaryKey must equalTo(Some("id"))
    tableDiff.compPrimaryKey must equalTo(Some("id, first_name"))

    compareResult.hasDifferences must beTrue

  }

  "show no primay key differences for different creation order  " in freshDB {

    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, last_name varchar(10), first_name varchar(10) , primary key(first_name, id))")
    executeWithDataStore(compareDatastore, "create table newtable01 (id bigint, last_name varchar(10), first_name varchar(10) , primary key(id, first_name))")

    val compareResult = PostgresCompare.compare(refDatastore, compareDatastore)
    val tableDiffs = compareResult.tableDiffs

    tableDiffs.size must equalTo(0)

    compareResult.hasDifferences must beFalse

  }



  "loadColumnDefs " in freshDB {
    val refDatasource = DataSourceUtil.createDataSourceFromDataStore(refDatastore)
    val conn = refDatasource.getConnection()
    val columnDefs = PostgresCompare.loadColumnDefs(conn, "public", "person")
    conn.close()
    refDatasource.close()

    columnDefs.size must equalTo(4)

    // check id column
    // change person table to make sure we can test all cases
    columnDefs(0).column_name must equalTo("id")
    columnDefs(0).data_type must equalTo("bigint")
    columnDefs(0).ordinal_position must equalTo(1)
    columnDefs(0).is_nullable must beFalse
    columnDefs(0).character_maximum_length must beNone
    columnDefs(0).numeric_precision must equalTo(Some(64))
    columnDefs(0).numeric_scale must equalTo(Some(0))
    columnDefs(0).column_default must beNone
    columnDefs(0).dataTypeString must equalTo("bigint (64,0)")

    // check firstName column
    columnDefs(1).column_name must equalTo("first_name")
    columnDefs(1).data_type must equalTo("character varying")
    columnDefs(1).ordinal_position must equalTo(2)
    columnDefs(1).is_nullable must beTrue
    columnDefs(1).character_maximum_length must beNone
    columnDefs(1).numeric_precision must beNone
    columnDefs(1).numeric_scale must beNone
    columnDefs(1).column_default must equalTo(Some("'daryl'::character varying"))
    columnDefs(1).dataTypeString must equalTo("character varying")

    // check lastName column
    columnDefs(2).column_name must equalTo("last_name")
    columnDefs(2).data_type must equalTo("character varying")
    columnDefs(2).ordinal_position must equalTo(3)
    columnDefs(2).is_nullable must beTrue
    columnDefs(2).character_maximum_length must equalTo(Some(40))
    columnDefs(2).numeric_precision must beNone
    columnDefs(2).numeric_scale must beNone
    columnDefs(2).column_default must beNone
    columnDefs(2).dataTypeString must equalTo("character varying (40)")

    // check money_in_pocket column
    columnDefs(3).column_name must equalTo("money_in_pocket")
    columnDefs(3).data_type must equalTo("numeric")
    columnDefs(3).ordinal_position must equalTo(4)
    columnDefs(3).is_nullable must beTrue
    columnDefs(3).character_maximum_length must beNone
    columnDefs(3).numeric_precision must equalTo(Some(3))
    columnDefs(3).numeric_scale must equalTo(Some(2))
    columnDefs(3).column_default must equalTo(Some("1.25"))
    columnDefs(3).dataTypeString must equalTo("numeric (3,2)")

  }

  "load primary keys " in freshDB {
    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, last_name varchar(10), first_name varchar(10) , primary key(id))")

    val refDatasource = DataSourceUtil.createDataSourceFromDataStore(refDatastore)
    val conn = refDatasource.getConnection()
    val primaryKey = PostgresCompare.loadPrimaryKey(conn, "public", "newtable01")
    conn.close()
    refDatasource.close()

    primaryKey must equalTo("id")

  }

  "load primary keys with composite primary key " in freshDB {
    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, last_name varchar(10), first_name varchar(10) , primary key(id, first_name))")

    val refDatasource = DataSourceUtil.createDataSourceFromDataStore(refDatastore)
    val conn = refDatasource.getConnection()
    val primaryKey = PostgresCompare.loadPrimaryKey(conn, "public", "newtable01")
    conn.close()
    refDatasource.close()

    primaryKey must equalTo("id, first_name")

  }

  "load primary keys with composite primary key in reverse order" in freshDB {
    executeWithDataStore(refDatastore, "create table newtable01 (id bigint, last_name varchar(10), first_name varchar(10) , primary key(first_name, id))")

    val refDatasource = DataSourceUtil.createDataSourceFromDataStore(refDatastore)
    val conn = refDatasource.getConnection()
    val primaryKey = PostgresCompare.loadPrimaryKey(conn, "public", "newtable01")
    conn.close()
    refDatasource.close()

    primaryKey must equalTo("id, first_name")

  }




  def executeWithDataStore (dataStore: Datastore, query : String) : Unit  = {
    val dataSource = DataSourceUtil.createDataSourceFromDataStore(dataStore)
    implicit val conn = dataSource.getConnection()
    SQL(query).execute()
    conn.close()
    dataSource.close()


  }


}
