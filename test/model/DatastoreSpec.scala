package model

import java.sql.Timestamp
import java.util.Date

import fixture.BackendSpecification
import test.TestConstants

class DatastoreSpec extends BackendSpecification {
  
  "Datastore model" should {


    // test get by id
    "be retrieved by id" in freshDB {

      val datastore = Datastore.getById(1).get

      datastore.id must equalTo(1)
      datastore.name must equalTo("Test 1.2.3 Reference")
      datastore.host must equalTo("127.0.0.1")
      datastore.port must equalTo(9999)
      datastore.username must equalTo("testuser")
      datastore.password must equalTo("testpassword")
      datastore.dbName must equalTo("test_ref")
      datastore.versionId must equalTo(1)
      datastore.version.id must equalTo(1)
      datastore.version.name must equalTo("1.2.3")

    }


    "insert" in freshDB {

      val datastore = Datastore(
        0L,
        "Test Db 2",
        "127.0.0.2",
        1234,
        "daryl",
        "myPass",
        "myDB",
        1,
        false
      )

      val dbDatastore = Datastore.insert(datastore)

      dbDatastore.id must equalTo(100)
      dbDatastore.name must equalTo("Test Db 2")
      dbDatastore.host must equalTo("127.0.0.2")
      dbDatastore.port must equalTo(1234)
      dbDatastore.username must equalTo("daryl")
      dbDatastore.password must equalTo("myPass")
      dbDatastore.dbName must equalTo("myDB")
      // dbDatastore.isReference must beFalse

    }

    "update" in freshDB {

      var datastore = Datastore.getById(1).get
      Datastore.update(datastore.copy(name = "New DB Name"))

      datastore = Datastore.getById(1).get

      datastore.name  must equalTo ("New DB Name")

    }


    "delete" in freshDB {

      val datastore = Datastore(
        0L,
        "Test Db 2",
        "127.0.0.2",
        1234,
        "daryl",
        "myPass",
        "myDB",
        1,
        false
      )

      val dbDatastore = Datastore.insert(datastore)

      Datastore.delete(dbDatastore.id)
      val datastoreOption = Datastore.getById(dbDatastore.id)
      datastoreOption must equalTo(None)
    }


    "get reference datastore" in freshDB {

      val datastore = Datastore.getReferenceDatastore(1L).get
      datastore.id must equalTo(1L)
      datastore.version.name must equalTo("1.2.3")
      datastore.isReference must beTrue
    }

    "return None if no reference datastore exists " in freshDB {
      val datastore = Datastore.getReferenceDatastore(TestConstants.DATASTORE_ID_V123_REF).get
      Datastore.update(datastore.copy(isReference = false))
      Datastore.getReferenceDatastore(TestConstants.DATASTORE_ID_V123_REF)  must beNone
    }

    "check if reference datastore exists  " in freshDB {
      Datastore.referenceDatastoreExists(TestConstants.VERSION_ID_V123, TestConstants.DATASTORE_ID_V123_REF) must beFalse
      Datastore.referenceDatastoreExists(TestConstants.VERSION_ID_V123, TestConstants.DATASTORE_ID_V123_NON_REF_2) must beTrue
      Datastore.referenceDatastoreExists(TestConstants.VERSION_ID_V124, TestConstants.DATASTORE_ID_V123_REF) must beTrue
      Datastore.referenceDatastoreExists(TestConstants.VERSION_ID_V125, TestConstants.DATASTORE_ID_V123_REF) must beFalse
      Datastore.referenceDatastoreExists(TestConstants.VERSION_ID_V123, 0L) must beTrue
      Datastore.referenceDatastoreExists(TestConstants.VERSION_ID_V125, 0L) must beFalse
    }




    
  }
  
}
