# --- !Ups
ALTER TABLE line
ADD COLUMN status varchar(15) NOT NULL DEFAULT 'ACTIVE';


# --- !Downs
ALTER TABLE line
drop COLUMN status;