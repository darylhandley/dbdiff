# --- !Ups
CREATE SEQUENCE datastore_id_seq;

CREATE TABLE datastore (
  id integer NOT NULL UNIQUE DEFAULT nextval('datastore_id_seq'),
  name varchar(255),
  host varchar(100),
  port numeric (5),
  username varchar(20),
  password varchar(20),
  db_name varchar(50)
);


# --- !Downs

DROP TABLE datastore;

DROP SEQUENCE datastore_id_seq;