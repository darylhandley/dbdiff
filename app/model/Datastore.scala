package model

import model.Database._
import org.squeryl.KeyedEntity
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.annotations.Column

/**
 * Created by darylhandley on 15-04-22.
 */
case class Datastore (
  id : Long,
  @Column("name") name : String,
  @Column("host") host : String,
  @Column("port") port: Int,
  @Column("username") username: String,
  @Column("password") password: String,
  @Column("db_name") dbName : String,
  @Column("version_id") versionId: Int,
  @Column("is_reference") isReference : Boolean
) extends KeyedEntity[Long] {

  lazy val version: Version = inTransaction {
    Database.versionToDatastore.right(this).toList(0)
  }

}

object Datastore {
  def getById(id: Long): Option[Datastore] = inTransaction {
    datastoreTable.lookup(id)
  }

  def insert(datastore: Datastore): Datastore = inTransaction {
    datastoreTable.insert(datastore)
  }

  def delete(id: Long): Boolean = inTransaction {
    datastoreTable.delete(id)
  }

  def update(datastore: Datastore) {
    inTransaction {
      datastoreTable.update(datastore)
    }
  }

  def all()  = inTransaction {
    from(datastoreTable) {datastore => select(datastore)}.toList
  }

  // returns true if a reference datastore exists that is not the same id as the one passed in and false otherwise
  def referenceDatastoreExists(versionId : Long, currentDatastoreId : Long): Boolean = {
    getReferenceDatastore(versionId) match {
      case Some(datastore) => {
        !(datastore.id == currentDatastoreId)
      }
      case None => false
    }
  }

  def getReferenceDatastore(versionId : Long): Option[Datastore] = inTransaction {

    val datastores = from(datastoreTable) {
      datastore =>
        where (datastore.versionId === versionId and datastore.isReference === true) select(datastore)
    }.toList

    if (datastores.isEmpty)  None else Some(datastores(0))
  }

}
