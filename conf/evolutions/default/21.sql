# --- !Ups
CREATE SEQUENCE version_id_seq;

CREATE TABLE version (
  id integer NOT NULL UNIQUE DEFAULT nextval('version_id_seq'),
  name varchar(255) not null
);


# --- !Downs

DROP TABLE version;

DROP SEQUENCE version_id_seq;