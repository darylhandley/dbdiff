package controllers

import java.sql.Timestamp
import java.util.Date

import controllers.DatastoresController._
import model._
import org.squeryl.annotations._
import play.Logger
import play.api.data.Forms._
import play.api.data._
import play.api.mvc._
import views._
import web.ProvidesHeader


/**
 * Allows user to create a new Datastore or edit details on an existing one.
 */
object ComparisonsController extends Controller with ProvidesHeader {

  val comparisonForm: Form[Comparison] = Form(

    // define mapping to handle datastore  values
    mapping(
      "referenceDatastoreId" -> longNumber(),
      "compareDatastoreId" -> longNumber()
    )

    {
      // Binding: Create a comparison from the mapping result
      (referenceDatastoreId, compareDatastoreId) => Comparison(
        0L,
        referenceDatastoreId,
        compareDatastoreId,
        "PENDING",
        "",
        new Timestamp(new Date().getTime()),
        None,
        None
      )
    }
    {
      // Unbinding: Create the mapping values from an existing comparison value
      // TODO: we probably don't need this as we are not going to allow edit of a comparison
      comparison => Some(comparison.referenceDatastoreId, comparison.compareDatastoreId)
    }.verifying(

    )
  )

  def list  = Action { implicit request =>
    Logger.info("Displaying all comparisons ")
    val comparisons = Comparison.all().sortBy(_.id).reverse
    Ok(views.html.comparisons(comparisons))
  }

  def create = Action { implicit request =>
    // this is just a dummy comparison for populating the form
    // TODO: in this case maybe we don't need to bind the object directly to the form
    // TODO: Maybe we don't need to prefill the form at all ?
    val comparison = Comparison(
      0L,
      0L,
      0L,
      "",
      "",
      new Timestamp(new Date().getTime()),
      None,
      None
    )
    val prefilledForm = comparisonForm.fill(comparison)
    Ok(views.html.comparisonsCreate(prefilledForm))
  }

  /** save action when form submitted */
  def save = Action { implicit request =>
    comparisonForm.bindFromRequest.fold(

      // Form has errors, redisplay it
      errors => BadRequest(views.html.comparisonsCreate(errors)),

      comparison => {
        Comparison.insert(comparison)
        Redirect(routes.ComparisonsController.list())
      }
    )
  }

  def view (id: Long) = Action { implicit request =>
    Logger.info("Displaying comparison result for  " + id)
    val comparison = Comparison.getById(id)
    Ok(views.html.comparisonsView(comparison.get))
  }

//  def delete (id: Long) = Action { implicit request =>
//    // TODO, check if the datastore exists
//    Version.delete(id)
//    Redirect(routes.VersionsController.list())
//  }

}