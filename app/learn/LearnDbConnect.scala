package learn

import javax.sql.DataSource

import anorm.SqlRow
import org.apache.commons.dbcp.BasicDataSource
import java.sql.Connection

/*
 *
 */
class LearnDbConnect {

}

object LearnDbConnect {

  def createMapRow (colName : String, row: SqlRow) (implicit conn: Connection) : Tuple2[String, AnyRef]  = {
    colName -> row[String](colName)
  }


  def main( args : Array[String]): Unit = {
    println("starting up")

//
    val dataSource: DataSource = {
      val ds = new BasicDataSource
      ds.setDriverClassName("org.postgresql.Driver")
      ds.setUsername("xm_mustafar")
      ds.setPassword("")
      ds.setMaxActive(20);
      ds.setMaxIdle(10);
      ds.setInitialSize(10);
      // ds.setValidationQuery("SELECT 1 FROM INFORMATION_SCHEMA.SYSTEM_USERS")
      // new java.io.File("target").mkdirs // ensure that folder for database exists
      ds.setUrl("jdbc:postgresql://localhost/xm_mustafar")
      ds
    }

    // test the data source validity
    implicit val conn = dataSource.getConnection()


    // create a datasource
    // def datasource  =
//    anorm.SQL("select * from persons").map(row =>
//      row[String]("targetName") -> row[String]("targetName")
//    )

//    val selectCountries  = anorm.SQL("select * from persons")
//    val countries : List[Tuple2[String, String]] = selectCountries().map(row =>
//      row[String]("first_name") -> row[String]("last_name")
//    ).toList

    // this works  but only for string columns
    val selectPersons = anorm.SQL("select first_name, last_name from persons")
    val persons : List[Map[String, AnyRef]] = selectPersons().map(row => {
        // implicit conn
        var rowMap: Map[String, AnyRef] = Map()
        row.metaData.availableColumns.map(colName => {
            // implicit conn = dataSource.getConnection()
            rowMap += createMapRow(colName, row)
          }
        )
        // row.data.foreach(println(_))

      // )
        rowMap
      }
    ).toList


    persons.foreach(person =>
      println(person("first_name"))
    )


    // get a conection from it and pass it to SQL from Anorm\
    conn.close()

    println("done")
  }



}
