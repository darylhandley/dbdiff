# --- !Ups
ALTER TABLE event_competitor
ADD COLUMN score integer NOT NULL DEFAULT 0;


# --- !Downs
ALTER TABLE event_competitor
drop COLUMN score;