package compare

object ColumnDiffType extends Enumeration {
  type ColumnDiffType = Value
  val NULLABILITY, DATATYPE, DEFAULT, MISSING_IN_REF, MISSING_IN_COMP = Value

}

