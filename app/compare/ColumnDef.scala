package compare

import java.sql.Connection

import scala.collection.immutable.HashSet

/**
 * Column definition. Holds the defition of a column as read from information_schema.columns
 */
case class ColumnDef (column_name : String,
                      data_type : String,
                      ordinal_position : Int,
                      is_nullable : Boolean,
                      character_maximum_length : Option[Int],
                      numeric_precision : Option[Int],
                      numeric_precision_radix : Option[Int],
                      numeric_scale : Option[Int],
                      datetime_precision : Option[Int],
                      is_identity : Boolean,
                      column_default : Option[String]
                      ) {

  def dataTypeString : String = {

    val sizeOrPrecision : String = {
      if (character_maximum_length.isDefined) {
        "(" + character_maximum_length.get + ")"
      } else if (numeric_precision.isDefined) {
        if (numeric_scale.isDefined) {
          "(" + numeric_precision.get + "," + numeric_scale.get + ")"
        } else {
          "(" + numeric_precision.get + ")"
        }
      } else {
          ""
      }
    }
    (data_type  + " " + sizeOrPrecision).trim
  }
}

