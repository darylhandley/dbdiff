# --- !Ups
ALTER TABLE event
ADD COLUMN start_time timestamp NULL;


# --- !Downs
ALTER TABLE event
drop COLUMN start_time;