# --- !Ups
alter  TABLE datastore  add column is_reference boolean not null default false;


# --- !Downs
alter table datastore drop column is_reference;
