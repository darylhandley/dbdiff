package controllers

import controllers.DatastoresController._
import model._
import play.Logger
import play.api.data.Forms._
import play.api.data._
import play.api.mvc._
import views._
import web.ProvidesHeader


/**
 * Allows user to create a new Datastore or edit details on an existing one.
 */
object VersionsController extends Controller with ProvidesHeader {

  val versionForm: Form[Version] = Form(

    // define mapping to handle datastore  values
    mapping(
      "id" -> optional(longNumber()),
      "name" -> text(minLength = 1)

      // tuple for password and confirm
//      "password" -> tuple(
//        "main" -> text(minLength = 6),
//        "confirm" -> text
//      ).verifying(
//        "Confirm password does not match password.", passwords => passwords._1 == passwords._2
//      )
    )
    // The mapping signature doesn't match the Player case class signature,
    // so we have to define custom binding/unbinding functions

    {
      // Binding: Create a datastore from the mapping result
      // it gets username, email, passwords from the mapping above, not sure how
      (id, name) => Version(id.getOrElse(0), name)
    }
    {
      // Unbinding: Create the mapping values from an existing datastore value
      version => Some(Some(version.id), version.name)
    }.verifying(
      // don't need this right now but keep it around for later, it will just prevent
      // usernames of admin or guest, could use it later maybe to
      // check for dupliate db names
      // check for duplicate reference datastores for same version
      // "This username is not available",
      // datastore => !Seq("admin", "guest").contains(datastore.name)
    )
  )

  def list  = Action { implicit request =>
    Logger.info("Displaying all versions ")
    val versions = Version.all().sortBy(_.id)
    Ok(views.html.versions("Versions", versions))
  }

  def create = Action { implicit request =>
    val version = Version(0, "")
    val prefilledForm = versionForm.fill(version)
    Ok(views.html.versionsEdit(prefilledForm, "Add Version"))
  }

  def edit (id: Long) = Action { implicit request =>
    // TODO, check if the datastore exists
    val version = Version.getById(id)
    val prefilledForm = versionForm.fill(version.get)
    Ok(views.html.versionsEdit(prefilledForm, "Edit Version"))
  }

  def save = Action { implicit request =>
    versionForm.bindFromRequest.fold(

      // Form has errors, redisplay it
      errors => BadRequest(views.html.versionsEdit(errors, "Blah")),

      // We got a valid datastore value, save it and display the summary
      version => {
        version.id match {
//          case Some(Long) => {
//            Datastore.update(datastore)
//          }
          case 0 => {
            Version.insert(version)
          }
          case _ => {
            Version.update(version)
          }
        }

        // Player.create(player.username, player.password, player.email)
        // Ok(html.addEditDatastoreSuccess("Datastore Saved", datastore))
        Redirect(routes.VersionsController.list())
      }
    )
  }

  def delete (id: Long) = Action { implicit request =>
    // TODO, check if the datastore exists
    Version.delete(id)
    Redirect(routes.VersionsController.list())
  }

}